﻿using System;
using System.Linq;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace EconSampleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                /* 
                 * Configurazione programmatica del client, da utilizzare per istanziare le classi `*Api.cs`.
                 * In alternativa, la configurazione può essere specificata come default globale 
                 * nella classe `Client/GlobalConfiguration.cs`.
                 */
                Configuration sampleConfiguration = new Configuration
                {
                    /* Url del server di test. 
                     * Il server di produzione risponde a https://entaksi.eu/api/edoc/v1 */
                    BasePath = "https://test.entaksi.eu/api/edoc/v1",
                    Username = "username",
                    Password = "password",
                    ClientID = "client-id",
                    Secret = "secret"
                };

                AziendeApi aziendeApi = new AziendeApi(sampleConfiguration);

                /* Creazione dell'azienda */
                Azienda acmeInc = aziendeApi.CreateCompany(new Azienda()
                {
                    Nome = "ACME",
                    Cognome = "Incorporated",
                    RagioneSociale = "ACME inc.",
                    /* La partita iva è obbligatoria se non si specifica il codice fiscale */
                    PartitaIva = "12345678901",
                    /* Il codice fiscale è obbligatorio se non si specifica la partita iva */
                    CodiceFiscale = "ABCDEF01G23H456I",
                    Email = "acme@example.net",
                    EmailPec = "acme-pec@example.net",
                    Indirizzo = "via Roma, 100",
                    Localita = "Napoli",
                    Paese = "IT",
                    Cap = "84000",
                    Provincia = "NA"
                });

                Console.WriteLine("azienda creata: '" + acmeInc.Nome + "'");

                UtentiApi utentiApi = new UtentiApi(sampleConfiguration);

                ElencoUtenti utenti = utentiApi.List();
                if (utenti.Utenti.Count > 0)
                {
                    Utente primoUtente = utenti.Utenti.First();

                    /* Aggiunta dell'utente all'azienda */
                    aziendeApi.AddUser(acmeInc.Id.ToString(), primoUtente.Id.ToString());

                    Console.WriteLine("utente '" + primoUtente.Username + "' aggiunto all'azienda' " + acmeInc.Nome + "'");
                }

                Console.WriteLine("elenco degli utenti per l'azienda' " + acmeInc.Nome + "':");
                ElencoUtenti utentiAcmeInc = aziendeApi.Users(acmeInc.Id.ToString());
                foreach (Utente u in utentiAcmeInc.Utenti)
                {
                    Console.WriteLine("- " + u.Username);
                }
            }
            catch (Entaksi.eCon.Client.ApiException ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
    }
}
