# Entaksi.eCon.Api.PdaApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Documents**](PdaApi.md#documents) | **GET** /pda/{idPda}/documenti | Recupera le informazioni di un pacchetto di archiviazione
[**History**](PdaApi.md#history) | **GET** /pda/{idPda}/storico | Storico del pacchetto di archiviazione
[**List**](PdaApi.md#list) | **GET** /pda | Recupera l&#39;elenco dei pacchetti di archiviazione
[**MetadataForDocument**](PdaApi.md#metadatafordocument) | **GET** /pda/{idPda}/documenti/{idDocumento} | Recupera i metadati di un documento contenuto in un pacchetto di archiviazione
[**Read**](PdaApi.md#read) | **GET** /pda/{idPda} | Recupera le informazioni di un pacchetto di archiviazione


<a name="documents"></a>
# **Documents**
> ElencoDocumenti Documents (long? idPda, int? inizio = null, int? max = null)

Recupera le informazioni di un pacchetto di archiviazione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class DocumentsExample
    {
        public void main()
        {
            var apiInstance = new PdaApi();
            var idPda = 789;  // long? | Identificativo del pacchetto di archiviazione
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Recupera le informazioni di un pacchetto di archiviazione
                ElencoDocumenti result = apiInstance.Documents(idPda, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdaApi.Documents: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPda** | **long?**| Identificativo del pacchetto di archiviazione | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoDocumenti**](ElencoDocumenti.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="history"></a>
# **History**
> ElencoStoriciPdv History (long? idPda, int? inizio = null, int? max = null)

Storico del pacchetto di archiviazione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class HistoryExample
    {
        public void main()
        {
            var apiInstance = new PdaApi();
            var idPda = 789;  // long? | Identificativo del pacchetto di archiviazione
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Storico del pacchetto di archiviazione
                ElencoStoriciPdv result = apiInstance.History(idPda, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdaApi.History: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPda** | **long?**| Identificativo del pacchetto di archiviazione | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoStoriciPdv**](ElencoStoriciPdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list"></a>
# **List**
> ElencoPda List (int? inizio = null, int? max = null, string produttore = null, string numero = null, string sezionale = null, string tipoDocumento = null, string anno = null, string stato = null, List<string> s = null)

Recupera l'elenco dei pacchetti di archiviazione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListExample
    {
        public void main()
        {
            var apiInstance = new PdaApi();
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var produttore = produttore_example;  // string | Ricerca per produttore (optional) 
            var numero = numero_example;  // string | Ricerca per numero pacchetto (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale (optional) 
            var tipoDocumento = tipoDocumento_example;  // string | Ricerca per tipo documento (optional) 
            var anno = anno_example;  // string | Ricerca per anno (optional) 
            var stato = stato_example;  // string | Stato pda (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco dei pacchetti di archiviazione
                ElencoPda result = apiInstance.List(inizio, max, produttore, numero, sezionale, tipoDocumento, anno, stato, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdaApi.List: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **produttore** | **string**| Ricerca per produttore | [optional] 
 **numero** | **string**| Ricerca per numero pacchetto | [optional] 
 **sezionale** | **string**| Ricerca per sezionale | [optional] 
 **tipoDocumento** | **string**| Ricerca per tipo documento | [optional] 
 **anno** | **string**| Ricerca per anno | [optional] 
 **stato** | **string**| Stato pda | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoPda**](ElencoPda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="metadatafordocument"></a>
# **MetadataForDocument**
> List<Object> MetadataForDocument (long? idPda, long? idDocumento)

Recupera i metadati di un documento contenuto in un pacchetto di archiviazione

Recupera i metadati di un documento contenuto in un pacchetto di archiviazione.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class MetadataForDocumentExample
    {
        public void main()
        {
            var apiInstance = new PdaApi();
            var idPda = 789;  // long? | Identificativo del pacchetto di archiviazione
            var idDocumento = 789;  // long? | Identificativo del documento contenuto nel pacchetto di archiviazione

            try
            {
                // Recupera i metadati di un documento contenuto in un pacchetto di archiviazione
                List&lt;Object&gt; result = apiInstance.MetadataForDocument(idPda, idDocumento);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdaApi.MetadataForDocument: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPda** | **long?**| Identificativo del pacchetto di archiviazione | 
 **idDocumento** | **long?**| Identificativo del documento contenuto nel pacchetto di archiviazione | 

### Return type

**List<Object>**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read"></a>
# **Read**
> Pda Read (long? idPda)

Recupera le informazioni di un pacchetto di archiviazione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadExample
    {
        public void main()
        {
            var apiInstance = new PdaApi();
            var idPda = 789;  // long? | Identificativo del pacchetto di archiviazione

            try
            {
                // Recupera le informazioni di un pacchetto di archiviazione
                Pda result = apiInstance.Read(idPda);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdaApi.Read: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPda** | **long?**| Identificativo del pacchetto di archiviazione | 

### Return type

[**Pda**](Pda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

