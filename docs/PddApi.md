# Entaksi.eCon.Api.PddApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddResult**](PddApi.md#addresult) | **POST** /pdd/{idPdd}/risultati | Aggiunge un risultato a un pacchetto di distribuzione
[**DeselectAllPddResults**](PddApi.md#deselectallpddresults) | **PUT** /pdd/{idPdd}/risultati/deselezionaTutti | Deseleziona tutti i risultati di una richiesta di PDD
[**History**](PddApi.md#history) | **GET** /pdd/{idPdd}/storico | Storico del pacchetto di distribuzione
[**List**](PddApi.md#list) | **GET** /pdd | Recupera l&#39;elenco dei pacchetti di distribuzione
[**ListResults**](PddApi.md#listresults) | **GET** /pdd/{idPdd}/risultati | Recupera l&#39;elenco dei risultati di un pacchetto di distribuzione
[**Read**](PddApi.md#read) | **GET** /pdd/{idPdd} | Recupera le informazioni di un pacchetto di distribuzione
[**RemoveResult**](PddApi.md#removeresult) | **DELETE** /pdd/{idPdd}/risultati/{idRisultato} | Rimuove un risultato da un pacchetto di distribuzione
[**Request**](PddApi.md#request) | **POST** /pdd/{idPdd}/richiedi | Porta il pacchetto di distribuzione nello stato PDD richiesto
[**RetrieveZip**](PddApi.md#retrievezip) | **GET** /pdd/{idPdd}/zip/{index} | Recupera il contenuto ZIP del PDD
[**SelectAllPddResults**](PddApi.md#selectallpddresults) | **PUT** /pdd/{idPdd}/risultati/selezionaTutti | Seleziona tutti i risultati di una richiesta di PDD
[**UpdatePdd**](PddApi.md#updatepdd) | **PUT** /pdd/{idPdd} | Aggiorna le informazioni di una richiesta di pacchetto di distribuzione
[**UpdatePddDescription**](PddApi.md#updatepdddescription) | **PUT** /pdd/{idPdd}/descrizione | Aggiorna la descrizione di una richiesta di pacchetto di distribuzione


<a name="addresult"></a>
# **AddResult**
> Pdd AddResult (long? idPdd, RisultatoPdd body = null)

Aggiunge un risultato a un pacchetto di distribuzione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class AddResultExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo della richiesta di pacchetto di distribuzione
            var body = new RisultatoPdd(); // RisultatoPdd | Informazioni sul risultato da aggiungere (optional) 

            try
            {
                // Aggiunge un risultato a un pacchetto di distribuzione
                Pdd result = apiInstance.AddResult(idPdd, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.AddResult: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo della richiesta di pacchetto di distribuzione | 
 **body** | [**RisultatoPdd**](RisultatoPdd.md)| Informazioni sul risultato da aggiungere | [optional] 

### Return type

[**Pdd**](Pdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deselectallpddresults"></a>
# **DeselectAllPddResults**
> RisultatoPdd DeselectAllPddResults (long? idPdd)

Deseleziona tutti i risultati di una richiesta di PDD

Deseleziona tutti i risultati di una richiesta di PDD.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class DeselectAllPddResultsExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo del risultato

            try
            {
                // Deseleziona tutti i risultati di una richiesta di PDD
                RisultatoPdd result = apiInstance.DeselectAllPddResults(idPdd);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.DeselectAllPddResults: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo del risultato | 

### Return type

[**RisultatoPdd**](RisultatoPdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="history"></a>
# **History**
> ElencoStoriciPdv History (long? idPdd, int? inizio = null, int? max = null)

Storico del pacchetto di distribuzione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class HistoryExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo della richiesta di pacchetto di distribuzione
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Storico del pacchetto di distribuzione
                ElencoStoriciPdv result = apiInstance.History(idPdd, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.History: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo della richiesta di pacchetto di distribuzione | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoStoriciPdv**](ElencoStoriciPdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list"></a>
# **List**
> ElencoPdd List (int? inizio = null, int? max = null, string tipo = null, string stato = null, string descrizione = null, List<string> s = null)

Recupera l'elenco dei pacchetti di distribuzione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var tipo = tipo_example;  // string | Tipo della richiesta  (optional) 
            var stato = stato_example;  // string | Stato della richiesta  (optional) 
            var descrizione = descrizione_example;  // string | Descrizione della richiesta  (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco dei pacchetti di distribuzione
                ElencoPdd result = apiInstance.List(inizio, max, tipo, stato, descrizione, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.List: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **tipo** | **string**| Tipo della richiesta  | [optional] 
 **stato** | **string**| Stato della richiesta  | [optional] 
 **descrizione** | **string**| Descrizione della richiesta  | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoPdd**](ElencoPdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listresults"></a>
# **ListResults**
> ElencoRisultatiPdd ListResults (long? idPdd, int? inizio = null, int? max = null, string uri = null, string titolo = null, string soggetto = null, string tipo = null, string flag = null, List<string> s = null)

Recupera l'elenco dei risultati di un pacchetto di distribuzione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListResultsExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo del pacchetto di distribuzione
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var uri = uri_example;  // string | Ricerca per uri (optional) 
            var titolo = titolo_example;  // string | Ricerca per titolo (optional) 
            var soggetto = soggetto_example;  // string | Ricerca per soggetto (optional) 
            var tipo = tipo_example;  // string | Ricerca per tipo (optional) 
            var flag = flag_example;  // string | Flag selezionato (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco dei risultati di un pacchetto di distribuzione
                ElencoRisultatiPdd result = apiInstance.ListResults(idPdd, inizio, max, uri, titolo, soggetto, tipo, flag, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.ListResults: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo del pacchetto di distribuzione | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **uri** | **string**| Ricerca per uri | [optional] 
 **titolo** | **string**| Ricerca per titolo | [optional] 
 **soggetto** | **string**| Ricerca per soggetto | [optional] 
 **tipo** | **string**| Ricerca per tipo | [optional] 
 **flag** | **string**| Flag selezionato | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoRisultatiPdd**](ElencoRisultatiPdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read"></a>
# **Read**
> Pdd Read (long? idPdd)

Recupera le informazioni di un pacchetto di distribuzione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo del pacchetto di distribuzione

            try
            {
                // Recupera le informazioni di un pacchetto di distribuzione
                Pdd result = apiInstance.Read(idPdd);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.Read: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo del pacchetto di distribuzione | 

### Return type

[**Pdd**](Pdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removeresult"></a>
# **RemoveResult**
> Pdd RemoveResult (long? idPdd, long? idRisultato)

Rimuove un risultato da un pacchetto di distribuzione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RemoveResultExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo della richiesta di pacchetto di distribuzione
            var idRisultato = 789;  // long? | Identificativo del risultato

            try
            {
                // Rimuove un risultato da un pacchetto di distribuzione
                Pdd result = apiInstance.RemoveResult(idPdd, idRisultato);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.RemoveResult: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo della richiesta di pacchetto di distribuzione | 
 **idRisultato** | **long?**| Identificativo del risultato | 

### Return type

[**Pdd**](Pdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="request"></a>
# **Request**
> Info Request (long? idPdd)

Porta il pacchetto di distribuzione nello stato PDD richiesto

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RequestExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo del pacchetto di distribuzione

            try
            {
                // Porta il pacchetto di distribuzione nello stato PDD richiesto
                Info result = apiInstance.Request(idPdd);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.Request: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo del pacchetto di distribuzione | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="retrievezip"></a>
# **RetrieveZip**
> System.IO.Stream RetrieveZip (long? idPdd, int? index)

Recupera il contenuto ZIP del PDD

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RetrieveZipExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo del pacchetto di distribuzione
            var index = 56;  // int? | Indice dello zip del pacchetto di distribuzione

            try
            {
                // Recupera il contenuto ZIP del PDD
                System.IO.Stream result = apiInstance.RetrieveZip(idPdd, index);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.RetrieveZip: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo del pacchetto di distribuzione | 
 **index** | **int?**| Indice dello zip del pacchetto di distribuzione | 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json, application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="selectallpddresults"></a>
# **SelectAllPddResults**
> RisultatoPdd SelectAllPddResults (long? idPdd)

Seleziona tutti i risultati di una richiesta di PDD

Seleziona tutti i risultati di una richiesta di PDD.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class SelectAllPddResultsExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo del risultato

            try
            {
                // Seleziona tutti i risultati di una richiesta di PDD
                RisultatoPdd result = apiInstance.SelectAllPddResults(idPdd);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.SelectAllPddResults: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo del risultato | 

### Return type

[**RisultatoPdd**](RisultatoPdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatepdd"></a>
# **UpdatePdd**
> Pdd UpdatePdd (long? idPdd, Pdd body = null, string salva = null)

Aggiorna le informazioni di una richiesta di pacchetto di distribuzione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UpdatePddExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo della richiesta di pacchetto di distribuzione
            var body = new Pdd(); // Pdd | Informazioni sul pacchetto di distribuzione da aggiornare (optional) 
            var salva = salva_example;  // string | Flag che indica se la richiesta è da salvare o da lasciare in bozza (optional) 

            try
            {
                // Aggiorna le informazioni di una richiesta di pacchetto di distribuzione
                Pdd result = apiInstance.UpdatePdd(idPdd, body, salva);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.UpdatePdd: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo della richiesta di pacchetto di distribuzione | 
 **body** | [**Pdd**](Pdd.md)| Informazioni sul pacchetto di distribuzione da aggiornare | [optional] 
 **salva** | **string**| Flag che indica se la richiesta è da salvare o da lasciare in bozza | [optional] 

### Return type

[**Pdd**](Pdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatepdddescription"></a>
# **UpdatePddDescription**
> Pdd UpdatePddDescription (long? idPdd, string body = null)

Aggiorna la descrizione di una richiesta di pacchetto di distribuzione

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UpdatePddDescriptionExample
    {
        public void main()
        {
            var apiInstance = new PddApi();
            var idPdd = 789;  // long? | Identificativo della richiesta di pacchetto di distribuzione
            var body = body_example;  // string | Informazioni sul pacchetto di distribuzione da aggiornare (optional) 

            try
            {
                // Aggiorna la descrizione di una richiesta di pacchetto di distribuzione
                Pdd result = apiInstance.UpdatePddDescription(idPdd, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PddApi.UpdatePddDescription: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdd** | **long?**| Identificativo della richiesta di pacchetto di distribuzione | 
 **body** | **string**| Informazioni sul pacchetto di distribuzione da aggiornare | [optional] 

### Return type

[**Pdd**](Pdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

