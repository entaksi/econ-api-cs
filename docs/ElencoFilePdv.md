# Entaksi.eCon.Model.ElencoFilePdv
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Paginazione** | [**Paginazione**](Paginazione.md) |  | [optional] 
**ListaFilePdv** | [**List&lt;FilePdv&gt;**](FilePdv.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

