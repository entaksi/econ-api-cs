# Entaksi.eCon.Api.RisultatiApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**EditPddResult**](RisultatiApi.md#editpddresult) | **PUT** /risultati/{idRisultato} | Modifica il risultato di una richiesta di PDD


<a name="editpddresult"></a>
# **EditPddResult**
> RisultatoPdd EditPddResult (long? idRisultato, RisultatoPdd body = null)

Modifica il risultato di una richiesta di PDD

Modifica il risultato di una richiesta di PDD.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class EditPddResultExample
    {
        public void main()
        {
            var apiInstance = new RisultatiApi();
            var idRisultato = 789;  // long? | Identificativo del risultato
            var body = new RisultatoPdd(); // RisultatoPdd | Informazioni sul risultato da modificare (optional) 

            try
            {
                // Modifica il risultato di una richiesta di PDD
                RisultatoPdd result = apiInstance.EditPddResult(idRisultato, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RisultatiApi.EditPddResult: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRisultato** | **long?**| Identificativo del risultato | 
 **body** | [**RisultatoPdd**](RisultatoPdd.md)| Informazioni sul risultato da modificare | [optional] 

### Return type

[**RisultatoPdd**](RisultatoPdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

