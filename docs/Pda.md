# Entaksi.eCon.Model.Pda
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**IdAzienda** | **long?** | Azienda che a cui fa riferimento il pacchetto | [optional] 
**Produttore** | **string** | Produttore a cui fa riferimento il pacchetto di archiviazione | [optional] 
**DataCreazione** | **DateTime?** | Data in cui il pacchetto di archiviazione è stato creato | [optional] 
**DataElaborazione** | **DateTime?** | Data in cui il pacchetto di archiviazione è stato elaborato/chiuso | [optional] 
**Stato** | **string** | Stato del pacchetto di archiviazione | [optional] 
**Anno** | **int?** | Anno di riferimento dei documenti contenuti nel pacchetto di archiviazione | [optional] 
**Sezionale** | **string** | Sezionale di riferimento dei documenti contenuti nel pacchetto di archiviazione | [optional] 
**TipoDocumento** | **string** | Tipo documento | [optional] 
**Numero** | **int?** | Numero | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

