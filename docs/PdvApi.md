# Entaksi.eCon.Api.PdvApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Confirm**](PdvApi.md#confirm) | **POST** /pdv/{idPdv}/conferma | Conferma il pacchetto di versamento
[**Files**](PdvApi.md#files) | **GET** /pdv/{idPdv}/files | Recupera la la lista dei file allegati ad un pacchetto di versamento
[**History**](PdvApi.md#history) | **GET** /pdv/{idPdv}/storico | Storico del pacchetto di versamento
[**List**](PdvApi.md#list) | **GET** /pdv | Recupera l&#39;elenco dei pacchetti di versamento
[**Rdv**](PdvApi.md#rdv) | **GET** /pdv/{idPdv}/rdv | Recupera la ricevuta di versamento relativa ad un pacchetto di versamento
[**Read**](PdvApi.md#read) | **GET** /pdv/{idPdv} | Recupera le informazioni di un pacchetto di versamento
[**RetrieveZip**](PdvApi.md#retrievezip) | **GET** /pdv/{idPdv}/files/{id}/zip | Recupera il file allegato al pacchetto di versamento


<a name="confirm"></a>
# **Confirm**
> Pdv Confirm (long? idPdv, Conferma body = null)

Conferma il pacchetto di versamento

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ConfirmExample
    {
        public void main()
        {
            var apiInstance = new PdvApi();
            var idPdv = 789;  // long? | Identificativo del pacchetto di versamento
            var body = new Conferma(); // Conferma |  (optional) 

            try
            {
                // Conferma il pacchetto di versamento
                Pdv result = apiInstance.Confirm(idPdv, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdvApi.Confirm: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdv** | **long?**| Identificativo del pacchetto di versamento | 
 **body** | [**Conferma**](Conferma.md)|  | [optional] 

### Return type

[**Pdv**](Pdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="files"></a>
# **Files**
> ElencoFilePdv Files (long? idPdv)

Recupera la la lista dei file allegati ad un pacchetto di versamento

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class FilesExample
    {
        public void main()
        {
            var apiInstance = new PdvApi();
            var idPdv = 789;  // long? | Identificativo del pacchetto di versamento

            try
            {
                // Recupera la la lista dei file allegati ad un pacchetto di versamento
                ElencoFilePdv result = apiInstance.Files(idPdv);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdvApi.Files: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdv** | **long?**| Identificativo del pacchetto di versamento | 

### Return type

[**ElencoFilePdv**](ElencoFilePdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="history"></a>
# **History**
> ElencoStoriciPdv History (long? idPdv, int? inizio = null, int? max = null)

Storico del pacchetto di versamento

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class HistoryExample
    {
        public void main()
        {
            var apiInstance = new PdvApi();
            var idPdv = 789;  // long? | Identificativo del pacchetto di versamento
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Storico del pacchetto di versamento
                ElencoStoriciPdv result = apiInstance.History(idPdv, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdvApi.History: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdv** | **long?**| Identificativo del pacchetto di versamento | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoStoriciPdv**](ElencoStoriciPdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list"></a>
# **List**
> ElencoPdv List (int? inizio = null, int? max = null, string dataInizio = null, string dataFine = null, string stato = null, string formato = null, List<string> s = null)

Recupera l'elenco dei pacchetti di versamento

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListExample
    {
        public void main()
        {
            var apiInstance = new PdvApi();
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var dataInizio = dataInizio_example;  // string | Data inizio selezione (yyyy-mm-dd) (optional) 
            var dataFine = dataFine_example;  // string | Data fine selezione (yyyy-mm-dd) (optional) 
            var stato = stato_example;  // string | Stato del pacchetto (optional) 
            var formato = formato_example;  // string | Formato del pacchetto (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco dei pacchetti di versamento
                ElencoPdv result = apiInstance.List(inizio, max, dataInizio, dataFine, stato, formato, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdvApi.List: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **dataInizio** | **string**| Data inizio selezione (yyyy-mm-dd) | [optional] 
 **dataFine** | **string**| Data fine selezione (yyyy-mm-dd) | [optional] 
 **stato** | **string**| Stato del pacchetto | [optional] 
 **formato** | **string**| Formato del pacchetto | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoPdv**](ElencoPdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="rdv"></a>
# **Rdv**
> System.IO.Stream Rdv (long? idPdv)

Recupera la ricevuta di versamento relativa ad un pacchetto di versamento

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RdvExample
    {
        public void main()
        {
            var apiInstance = new PdvApi();
            var idPdv = 789;  // long? | Identificativo del pacchetto di versamento

            try
            {
                // Recupera la ricevuta di versamento relativa ad un pacchetto di versamento
                System.IO.Stream result = apiInstance.Rdv(idPdv);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdvApi.Rdv: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdv** | **long?**| Identificativo del pacchetto di versamento | 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read"></a>
# **Read**
> Pdv Read (long? idPdv)

Recupera le informazioni di un pacchetto di versamento

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadExample
    {
        public void main()
        {
            var apiInstance = new PdvApi();
            var idPdv = 789;  // long? | Identificativo del pacchetto di versamento

            try
            {
                // Recupera le informazioni di un pacchetto di versamento
                Pdv result = apiInstance.Read(idPdv);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdvApi.Read: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdv** | **long?**| Identificativo del pacchetto di versamento | 

### Return type

[**Pdv**](Pdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="retrievezip"></a>
# **RetrieveZip**
> System.IO.Stream RetrieveZip (long? idPdv, long? id)

Recupera il file allegato al pacchetto di versamento

Restituisce il file allegato al pacchetto di versamento.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RetrieveZipExample
    {
        public void main()
        {
            var apiInstance = new PdvApi();
            var idPdv = 789;  // long? | Identificativo del pacchetto di versamento
            var id = 789;  // long? | Posizione dell'allegato nella lista degli allegati del pacchetto di versamento

            try
            {
                // Recupera il file allegato al pacchetto di versamento
                System.IO.Stream result = apiInstance.RetrieveZip(idPdv, id);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling PdvApi.RetrieveZip: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idPdv** | **long?**| Identificativo del pacchetto di versamento | 
 **id** | **long?**| Posizione dell&#39;allegato nella lista degli allegati del pacchetto di versamento | 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json, application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

