# Entaksi.eCon.Model.ModelClient
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**IdClient** | **string** | Identificativo del client | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

