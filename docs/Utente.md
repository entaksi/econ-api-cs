# Entaksi.eCon.Model.Utente
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Username** | **string** | Identificativo dell&#39;utente per l&#39;accesso | [optional] 
**Email** | **string** | Indirizzo email dell&#39;utente | [optional] 
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**Nome** | **string** | Nome dell&#39;utente | [optional] 
**Cognome** | **string** | Cognome dell&#39;utente | [optional] 
**EmailPec** | **string** | Indirizzo email PEC dell&#39;utente | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

