# Entaksi.eCon.Model.Roles
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_Roles** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

