# Entaksi.eCon.Model.Metadato
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Nome** | **string** | Nome del metadato. | [optional] 
**Valore** | **string** | Titolo del metadato. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

