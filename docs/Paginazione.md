# Entaksi.eCon.Model.Paginazione
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Inizio** | **int?** | Posizione del primo elemento restituito | [optional] 
**Max** | **int?** | Numero massimo di elementi restituiti | [optional] 
**Conteggio** | **long?** | Numero totale di elementi | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

