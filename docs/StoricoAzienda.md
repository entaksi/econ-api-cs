# Entaksi.eCon.Model.StoricoAzienda
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**IdAzienda** | **long?** | Codice azienda di riferimento | [optional] 
**IdUtente** | **long?** | Codice dell&#39;utente che ha eseguito l&#39;operazione | [optional] 
**Utente** | **string** | Descrizione dell&#39;utente che ha eseguito l&#39;operazione | [optional] 
**DataOperazione** | **DateTime?** | La data in cui è stata eseguita l&#39;operazione | [optional] 
**Stato** | **string** | Stato in cui si trova l&#39;azienda alla data indicata | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

