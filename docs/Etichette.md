# Entaksi.eCon.Model.Etichette
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_Etichette** | **List&lt;string&gt;** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

