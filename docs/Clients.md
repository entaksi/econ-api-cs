# Entaksi.eCon.Model.Clients
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_Clients** | [**List&lt;ModelClient&gt;**](ModelClient.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

