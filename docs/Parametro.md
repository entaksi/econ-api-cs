# Entaksi.eCon.Model.Parametro
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Nome** | **string** | Codice del parametro di configurazione | [optional] 
**Valore** | **string** | Valore del parametro di configurazione | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

