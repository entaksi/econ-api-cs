# Entaksi.eCon.Model.RisultatoPdd
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**UriDocumento** | **string** | Uri del documento | [optional] 
**TitoloDocumento** | **string** | Titolo del documento | [optional] 
**SoggettoDocumento** | **string** | Soggetto del documento | [optional] 
**TipoDocumento** | **string** | Tipo del documento | [optional] 
**DataDocumento** | **DateTime?** | Data del documento | [optional] 
**UriPda** | **string** | Uri del pda | [optional] 
**Selezionato** | **bool?** | Indica se il risultato deve essere selezionato durante la formazione del PDD | [optional] 
**IdElementoArchivio** | **long?** | Id dell&#39;elemento dell&#39;archivio | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

