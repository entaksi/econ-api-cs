# Entaksi.eCon.Api.FattureApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddNamedValue**](FattureApi.md#addnamedvalue) | **POST** /fatture/{idFattura}/dati | Aggiunge un dato associato alla prima fattura contenuta nel file.
[**Cancel**](FattureApi.md#cancel) | **POST** /fatture/{idFattura}/annulla | Annulla l&#39;invio della fattura
[**ChangeNamedValue**](FattureApi.md#changenamedvalue) | **PUT** /fatture/{idFattura}/dati/{nome} | Modifica un dato associato alla prima fattura contenuta nel file.
[**ConfirmEC**](FattureApi.md#confirmec) | **POST** /fatture/{idFattura}/confermaEC | Conferma la o le notifiche Esito Committente precedentemente inserite e le invia al cedente/prestatore
[**CreateFromXml**](FattureApi.md#createfromxml) | **POST** /fatture/xml | Crea una nuova fattura caricando il contenuto XML
[**CreateReceipt**](FattureApi.md#createreceipt) | **POST** /fatture/{idFattura}/ricevute | Crea una notifica per la fattura. Possono essere create solo notifiche di tipo Esito Committente.
[**Csv**](FattureApi.md#csv) | **GET** /fatture.csv | Recupera l&#39;elenco delle fatture visibili dall&#39;utente in formato CSV
[**DeleteNamedValue**](FattureApi.md#deletenamedvalue) | **DELETE** /fatture/{idFattura}/dati/{nome} | Elimina un dato associato alla prima fattura contenuta nel file.
[**Flag**](FattureApi.md#flag) | **POST** /fatture/{idFattura}/flag | Imposta il flag sulla fattura
[**GetNamedValue**](FattureApi.md#getnamedvalue) | **GET** /fatture/{idFattura}/dati/{nome} | Recupera un dato associato alla prima fattura contenuta nel file.
[**History**](FattureApi.md#history) | **GET** /fatture/{idFattura}/storico | Storico della fattura
[**List**](FattureApi.md#list) | **GET** /fatture | Recupera l&#39;elenco delle fatture visibili dall&#39;utente
[**ListNamedValues**](FattureApi.md#listnamedvalues) | **GET** /fatture/{idFattura}/dati | Recupera i dati associati alla prima fattura contenuta nel file.
[**OutcomeInvoiceTotalizations**](FattureApi.md#outcomeinvoicetotalizations) | **GET** /fatture/totaliEsiti | Recupera i totali per esiti
[**Read**](FattureApi.md#read) | **GET** /fatture/{idFattura} | Recupera le informazioni di una fattura
[**Receipts**](FattureApi.md#receipts) | **GET** /fatture/{idFattura}/ricevute | Recupera l&#39;elenco delle ricevute della fattura
[**RemoveReceipts**](FattureApi.md#removereceipts) | **DELETE** /fatture/{idFattura}/elementi/{idElemento} | Cancella le notifiche di tipo Esito Committente per la fattura.
[**Resume**](FattureApi.md#resume) | **POST** /fatture/{idFattura}/ripristina | Ripristina una fattura annullata
[**RetrieveP7m**](FattureApi.md#retrievep7m) | **GET** /fatture/{idFattura}.xml.p7m | Recupera il contenuto P7M della Fattura Elettronica
[**RetrievePdf**](FattureApi.md#retrievepdf) | **GET** /fatture/{idFattura}.pdf | Recupera la stampa pdf della Fattura Elettronica
[**RetrieveXml**](FattureApi.md#retrievexml) | **GET** /fatture/{idFattura}.xml | Recupera il contenuto XML della Fattura Elettronica
[**RetrieveXmlDeprecated**](FattureApi.md#retrievexmldeprecated) | **GET** /fatture/{idFattura}/xml | Recupera il contenuto XML della Fattura Elettronica
[**Send**](FattureApi.md#send) | **POST** /fatture/{idFattura}/invia | Programma l&#39;invio della fattura al Sistema di Interscambio
[**StatusInvoiceTotalizations**](FattureApi.md#statusinvoicetotalizations) | **GET** /fatture/totaliStati | Recupera i totali per stati
[**Unflag**](FattureApi.md#unflag) | **POST** /fatture/{idFattura}/unflag | Rimuove il flag sulla fattura
[**Update**](FattureApi.md#update) | **PUT** /fatture/{idFattura} | Aggiorna le informazioni di una fattura
[**UpdateXml**](FattureApi.md#updatexml) | **PUT** /fatture/{idFattura}/xml | Aggiorna il contenuto XML della Fattura Elettronica


<a name="addnamedvalue"></a>
# **AddNamedValue**
> Dato AddNamedValue (string idFattura, string dir = null, Dato body = null)

Aggiunge un dato associato alla prima fattura contenuta nel file.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class AddNamedValueExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo della fattura singola.
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 
            var body = new Dato(); // Dato | Dato da aggiungere alla fattura (optional) 

            try
            {
                // Aggiunge un dato associato alla prima fattura contenuta nel file.
                Dato result = apiInstance.AddNamedValue(idFattura, dir, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.AddNamedValue: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo della fattura singola. | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 
 **body** | [**Dato**](Dato.md)| Dato da aggiungere alla fattura | [optional] 

### Return type

[**Dato**](Dato.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="cancel"></a>
# **Cancel**
> Info Cancel (string idFattura)

Annulla l'invio della fattura

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CancelExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura

            try
            {
                // Annulla l'invio della fattura
                Info result = apiInstance.Cancel(idFattura);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.Cancel: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="changenamedvalue"></a>
# **ChangeNamedValue**
> Dato ChangeNamedValue (string idFattura, string nome, string dir = null, Dato body = null)

Modifica un dato associato alla prima fattura contenuta nel file.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ChangeNamedValueExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo della fattura singola.
            var nome = nome_example;  // string | Nome del dato da modificare.
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 
            var body = new Dato(); // Dato | Dato da modificare sulla fattura (optional) 

            try
            {
                // Modifica un dato associato alla prima fattura contenuta nel file.
                Dato result = apiInstance.ChangeNamedValue(idFattura, nome, dir, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.ChangeNamedValue: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo della fattura singola. | 
 **nome** | **string**| Nome del dato da modificare. | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 
 **body** | [**Dato**](Dato.md)| Dato da modificare sulla fattura | [optional] 

### Return type

[**Dato**](Dato.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="confirmec"></a>
# **ConfirmEC**
> Info ConfirmEC (string idFattura)

Conferma la o le notifiche Esito Committente precedentemente inserite e le invia al cedente/prestatore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ConfirmECExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura

            try
            {
                // Conferma la o le notifiche Esito Committente precedentemente inserite e le invia al cedente/prestatore
                Info result = apiInstance.ConfirmEC(idFattura);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.ConfirmEC: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createfromxml"></a>
# **CreateFromXml**
> Fattura CreateFromXml (List<byte[]> body = null)

Crea una nuova fattura caricando il contenuto XML

Il servizio accetta fatture nel formato XML FatturaPA ricavando l'azienda dalla partita IVA indicata nei dati del Cedente/Prestatore.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateFromXmlExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var body = ;  // List<byte[]> | xml (optional) 

            try
            {
                // Crea una nuova fattura caricando il contenuto XML
                Fattura result = apiInstance.CreateFromXml(body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.CreateFromXml: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **List&lt;byte[]&gt;**| xml | [optional] 

### Return type

[**Fattura**](Fattura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createreceipt"></a>
# **CreateReceipt**
> Ricevuta CreateReceipt (string idFattura, Ricevuta body = null)

Crea una notifica per la fattura. Possono essere create solo notifiche di tipo Esito Committente.

Possono essere create solo notifiche Esito Committente. Per le fatture a lotti la notifica EC può essere unica, senza cioè l'indicazione della specifica fattura, e in questo caso l'accettazione e il rifiuto valgono per l'intero lotto, oppure è possibile creare una notifica per ciascuna fattura contenuta nel lotto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateReceiptExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var body = new Ricevuta(); // Ricevuta |  (optional) 

            try
            {
                // Crea una notifica per la fattura. Possono essere create solo notifiche di tipo Esito Committente.
                Ricevuta result = apiInstance.CreateReceipt(idFattura, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.CreateReceipt: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **body** | [**Ricevuta**](Ricevuta.md)|  | [optional] 

### Return type

[**Ricevuta**](Ricevuta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="csv"></a>
# **Csv**
> System.IO.Stream Csv (string anno = null, string dataInizio = null, string dataFine = null, string numeroDocumento = null, string numero = null, string sezionale = null, string nomeFile = null, string destinatario = null, string mittente = null, string importo = null, string dir = null, string status = null, string esito = null, string schema = null, string flag = null, List<string> s = null, List<string> d = null)

Recupera l'elenco delle fatture visibili dall'utente in formato CSV

Restituisce l'elenco delle fatture visibili dall'utente in formato CSV.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CsvExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var anno = anno_example;  // string | Ricerca per anno (optional) 
            var dataInizio = dataInizio_example;  // string | Data documento inizio (yyyy-mm-dd) (optional) 
            var dataFine = dataFine_example;  // string | Data documento fine (yyyy-mm-dd) (optional) 
            var numeroDocumento = numeroDocumento_example;  // string | Ricerca per numero documento (optional) 
            var numero = numero_example;  // string | Ricerca per numero fattura (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale fattura (optional) 
            var nomeFile = nomeFile_example;  // string | Ricerca per nome file (optional) 
            var destinatario = destinatario_example;  // string | Ricerca per destinatario (optional) 
            var mittente = mittente_example;  // string | Ricerca per mittente (optional) 
            var importo = importo_example;  // string | Ricerca per importo (optional) 
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 
            var status = status_example;  // string | Stato fattura (optional) 
            var esito = esito_example;  // string | Esito fattura (optional) 
            var schema = schema_example;  // string | Schema fattura (optional) 
            var flag = flag_example;  // string | Flag modificato (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 
            var d = new List<string>(); // List<string> | Dato aggiuntivo (optional) 

            try
            {
                // Recupera l'elenco delle fatture visibili dall'utente in formato CSV
                System.IO.Stream result = apiInstance.Csv(anno, dataInizio, dataFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, dir, status, esito, schema, flag, s, d);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.Csv: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anno** | **string**| Ricerca per anno | [optional] 
 **dataInizio** | **string**| Data documento inizio (yyyy-mm-dd) | [optional] 
 **dataFine** | **string**| Data documento fine (yyyy-mm-dd) | [optional] 
 **numeroDocumento** | **string**| Ricerca per numero documento | [optional] 
 **numero** | **string**| Ricerca per numero fattura | [optional] 
 **sezionale** | **string**| Ricerca per sezionale fattura | [optional] 
 **nomeFile** | **string**| Ricerca per nome file | [optional] 
 **destinatario** | **string**| Ricerca per destinatario | [optional] 
 **mittente** | **string**| Ricerca per mittente | [optional] 
 **importo** | **string**| Ricerca per importo | [optional] 
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 
 **status** | **string**| Stato fattura | [optional] 
 **esito** | **string**| Esito fattura | [optional] 
 **schema** | **string**| Schema fattura | [optional] 
 **flag** | **string**| Flag modificato | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 
 **d** | [**List&lt;string&gt;**](string.md)| Dato aggiuntivo | [optional] 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv, application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletenamedvalue"></a>
# **DeleteNamedValue**
> Info DeleteNamedValue (string idFattura, string nome, string dir = null)

Elimina un dato associato alla prima fattura contenuta nel file.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class DeleteNamedValueExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo della fattura singola.
            var nome = nome_example;  // string | Nome del dato da eliminare.
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 

            try
            {
                // Elimina un dato associato alla prima fattura contenuta nel file.
                Info result = apiInstance.DeleteNamedValue(idFattura, nome, dir);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.DeleteNamedValue: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo della fattura singola. | 
 **nome** | **string**| Nome del dato da eliminare. | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="flag"></a>
# **Flag**
> Info Flag (string idFattura, string dir = null)

Imposta il flag sulla fattura

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class FlagExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 

            try
            {
                // Imposta il flag sulla fattura
                Info result = apiInstance.Flag(idFattura, dir);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.Flag: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getnamedvalue"></a>
# **GetNamedValue**
> Dato GetNamedValue (string idFattura, string nome, string dir = null)

Recupera un dato associato alla prima fattura contenuta nel file.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetNamedValueExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo della fattura singola.
            var nome = nome_example;  // string | Nome del dato
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 

            try
            {
                // Recupera un dato associato alla prima fattura contenuta nel file.
                Dato result = apiInstance.GetNamedValue(idFattura, nome, dir);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.GetNamedValue: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo della fattura singola. | 
 **nome** | **string**| Nome del dato | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 

### Return type

[**Dato**](Dato.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="history"></a>
# **History**
> ElencoStoriciFattura History (string idFattura, string dir = null, int? inizio = null, int? max = null)

Storico della fattura

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class HistoryExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Storico della fattura
                ElencoStoriciFattura result = apiInstance.History(idFattura, dir, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.History: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoStoriciFattura**](ElencoStoriciFattura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list"></a>
# **List**
> ElencoFatture List (int? inizio = null, int? max = null, string dataInizio = null, string dataFine = null, string dataIrInizio = null, string dataIrFine = null, string numeroDocumento = null, string numero = null, string sezionale = null, string nomeFile = null, string destinatario = null, string mittente = null, string importo = null, string dir = null, string status = null, string esito = null, string schema = null, string flag = null, string i = null, List<string> s = null)

Recupera l'elenco delle fatture visibili dall'utente

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var dataInizio = dataInizio_example;  // string | Data documento inizio (yyyy-mm-dd) (optional) 
            var dataFine = dataFine_example;  // string | Data documento fine (yyyy-mm-dd) (optional) 
            var dataIrInizio = dataIrInizio_example;  // string | Data invio/ricezione inizio (yyyy-mm-dd) (optional) 
            var dataIrFine = dataIrFine_example;  // string | Data invio/ricezione fine (yyyy-mm-dd) (optional) 
            var numeroDocumento = numeroDocumento_example;  // string | Ricerca per numero documento (optional) 
            var numero = numero_example;  // string | Ricerca per numero fattura (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale fattura (optional) 
            var nomeFile = nomeFile_example;  // string | Ricerca per nome file (optional) 
            var destinatario = destinatario_example;  // string | Ricerca per destinatario (optional) 
            var mittente = mittente_example;  // string | Ricerca per mittente (optional) 
            var importo = importo_example;  // string | Ricerca per importo (optional) 
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 
            var status = status_example;  // string | Stato fattura (optional) 
            var esito = esito_example;  // string | Esito fattura (optional) 
            var schema = schema_example;  // string | Schema fattura (optional) 
            var flag = flag_example;  // string | Flag modificato (optional) 
            var i = i_example;  // string | Campi supplementari da includere (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco delle fatture visibili dall'utente
                ElencoFatture result = apiInstance.List(inizio, max, dataInizio, dataFine, dataIrInizio, dataIrFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, dir, status, esito, schema, flag, i, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.List: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **dataInizio** | **string**| Data documento inizio (yyyy-mm-dd) | [optional] 
 **dataFine** | **string**| Data documento fine (yyyy-mm-dd) | [optional] 
 **dataIrInizio** | **string**| Data invio/ricezione inizio (yyyy-mm-dd) | [optional] 
 **dataIrFine** | **string**| Data invio/ricezione fine (yyyy-mm-dd) | [optional] 
 **numeroDocumento** | **string**| Ricerca per numero documento | [optional] 
 **numero** | **string**| Ricerca per numero fattura | [optional] 
 **sezionale** | **string**| Ricerca per sezionale fattura | [optional] 
 **nomeFile** | **string**| Ricerca per nome file | [optional] 
 **destinatario** | **string**| Ricerca per destinatario | [optional] 
 **mittente** | **string**| Ricerca per mittente | [optional] 
 **importo** | **string**| Ricerca per importo | [optional] 
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 
 **status** | **string**| Stato fattura | [optional] 
 **esito** | **string**| Esito fattura | [optional] 
 **schema** | **string**| Schema fattura | [optional] 
 **flag** | **string**| Flag modificato | [optional] 
 **i** | **string**| Campi supplementari da includere | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoFatture**](ElencoFatture.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listnamedvalues"></a>
# **ListNamedValues**
> ElencoDati ListNamedValues (string idFattura, string dir = null, int? inizio = null, int? max = null)

Recupera i dati associati alla prima fattura contenuta nel file.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListNamedValuesExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo della fattura.
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Recupera i dati associati alla prima fattura contenuta nel file.
                ElencoDati result = apiInstance.ListNamedValues(idFattura, dir, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.ListNamedValues: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo della fattura. | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoDati**](ElencoDati.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="outcomeinvoicetotalizations"></a>
# **OutcomeInvoiceTotalizations**
> ElencoValori OutcomeInvoiceTotalizations (string dir = null)

Recupera i totali per esiti

Recupera i totali per esiti.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class OutcomeInvoiceTotalizationsExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 

            try
            {
                // Recupera i totali per esiti
                ElencoValori result = apiInstance.OutcomeInvoiceTotalizations(dir);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.OutcomeInvoiceTotalizations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 

### Return type

[**ElencoValori**](ElencoValori.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read"></a>
# **Read**
> Fattura Read (string idFattura, string dir = null, string i = null)

Recupera le informazioni di una fattura

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 
            var i = i_example;  // string | Campi supplementari da includere (optional) 

            try
            {
                // Recupera le informazioni di una fattura
                Fattura result = apiInstance.Read(idFattura, dir, i);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.Read: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 
 **i** | **string**| Campi supplementari da includere | [optional] 

### Return type

[**Fattura**](Fattura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="receipts"></a>
# **Receipts**
> ElencoRicevute Receipts (string idFattura, string dir = null, int? inizio = null, int? max = null, string dataInizio = null, string dataFine = null, string tipo = null, string flag = null)

Recupera l'elenco delle ricevute della fattura

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReceiptsExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var dataInizio = dataInizio_example;  // string | Data inizio selezione ISO-8601 (optional) 
            var dataFine = dataFine_example;  // string | Data fine selezione ISO-8601 (optional) 
            var tipo = tipo_example;  // string | Tipo ricevuta (optional) 
            var flag = flag_example;  // string | Flag lettura (optional) 

            try
            {
                // Recupera l'elenco delle ricevute della fattura
                ElencoRicevute result = apiInstance.Receipts(idFattura, dir, inizio, max, dataInizio, dataFine, tipo, flag);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.Receipts: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **dataInizio** | **string**| Data inizio selezione ISO-8601 | [optional] 
 **dataFine** | **string**| Data fine selezione ISO-8601 | [optional] 
 **tipo** | **string**| Tipo ricevuta | [optional] 
 **flag** | **string**| Flag lettura | [optional] 

### Return type

[**ElencoRicevute**](ElencoRicevute.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removereceipts"></a>
# **RemoveReceipts**
> Info RemoveReceipts (string idFattura, long? idElemento)

Cancella le notifiche di tipo Esito Committente per la fattura.

Possono essere cancellate solo notifiche Esito Committente.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RemoveReceiptsExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var idElemento = 789;  // long? | Identificativo dell'elemento

            try
            {
                // Cancella le notifiche di tipo Esito Committente per la fattura.
                Info result = apiInstance.RemoveReceipts(idFattura, idElemento);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.RemoveReceipts: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **idElemento** | **long?**| Identificativo dell&#39;elemento | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="resume"></a>
# **Resume**
> Info Resume (string idFattura)

Ripristina una fattura annullata

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ResumeExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura

            try
            {
                // Ripristina una fattura annullata
                Info result = apiInstance.Resume(idFattura);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.Resume: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="retrievep7m"></a>
# **RetrieveP7m**
> System.IO.Stream RetrieveP7m (string idFattura, string dir = null)

Recupera il contenuto P7M della Fattura Elettronica

Restituisce il contenuto P7M della Fattura Elettronica.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RetrieveP7mExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 

            try
            {
                // Recupera il contenuto P7M della Fattura Elettronica
                System.IO.Stream result = apiInstance.RetrieveP7m(idFattura, dir);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.RetrieveP7m: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json, application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="retrievepdf"></a>
# **RetrievePdf**
> System.IO.Stream RetrievePdf (string idFattura, string dir = null, string stile = null)

Recupera la stampa pdf della Fattura Elettronica

Restituisce la stampa pdf della Fattura Elettronica.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RetrievePdfExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 
            var stile = stile_example;  // string | Foglio di stile (optional) 

            try
            {
                // Recupera la stampa pdf della Fattura Elettronica
                System.IO.Stream result = apiInstance.RetrievePdf(idFattura, dir, stile);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.RetrievePdf: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 
 **stile** | **string**| Foglio di stile | [optional] 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json, application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="retrievexml"></a>
# **RetrieveXml**
> System.IO.Stream RetrieveXml (string idFattura, string dir = null)

Recupera il contenuto XML della Fattura Elettronica

Restituisce il contenuto XML della Fattura Elettronica.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RetrieveXmlExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 

            try
            {
                // Recupera il contenuto XML della Fattura Elettronica
                System.IO.Stream result = apiInstance.RetrieveXml(idFattura, dir);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.RetrieveXml: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="retrievexmldeprecated"></a>
# **RetrieveXmlDeprecated**
> string RetrieveXmlDeprecated (string idFattura, string dir = null)

Recupera il contenuto XML della Fattura Elettronica

Restituisce il contenuto XML della Fattura Elettronica.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RetrieveXmlDeprecatedExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 

            try
            {
                // Recupera il contenuto XML della Fattura Elettronica
                string result = apiInstance.RetrieveXmlDeprecated(idFattura, dir);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.RetrieveXmlDeprecated: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="send"></a>
# **Send**
> Info Send (string idFattura)

Programma l'invio della fattura al Sistema di Interscambio

Programma l'invio della fattura al Sistema di Interscambio. Dopo questa operazione non sarà più possibile modificare il contento XML e il sistema client deve monitorare le ricevute per verificare se la fattura è stata accettata e consegnata.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class SendExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura

            try
            {
                // Programma l'invio della fattura al Sistema di Interscambio
                Info result = apiInstance.Send(idFattura);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.Send: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="statusinvoicetotalizations"></a>
# **StatusInvoiceTotalizations**
> ElencoValori StatusInvoiceTotalizations ()

Recupera i totali per stati

Recupera i totali per stati.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class StatusInvoiceTotalizationsExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();

            try
            {
                // Recupera i totali per stati
                ElencoValori result = apiInstance.StatusInvoiceTotalizations();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.StatusInvoiceTotalizations: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ElencoValori**](ElencoValori.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="unflag"></a>
# **Unflag**
> Info Unflag (string idFattura, string dir = null)

Rimuove il flag sulla fattura

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UnflagExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 

            try
            {
                // Rimuove il flag sulla fattura
                Info result = apiInstance.Unflag(idFattura, dir);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.Unflag: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update"></a>
# **Update**
> Fattura Update (string idFattura, string dir = null, Fattura body = null)

Aggiorna le informazioni di una fattura

Aggiorna le informazioni della fattura.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UpdateExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 
            var body = new Fattura(); // Fattura |  (optional) 

            try
            {
                // Aggiorna le informazioni di una fattura
                Fattura result = apiInstance.Update(idFattura, dir, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.Update: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 
 **body** | [**Fattura**](Fattura.md)|  | [optional] 

### Return type

[**Fattura**](Fattura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="updatexml"></a>
# **UpdateXml**
> Fattura UpdateXml (string idFattura, string dir = null, List<byte[]> body = null)

Aggiorna il contenuto XML della Fattura Elettronica

Aggiorna il contenuto XML della Fattura Elettronica. L'operazione è possibile solo se la fattura non è già stata elaborata.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UpdateXmlExample
    {
        public void main()
        {
            var apiInstance = new FattureApi();
            var idFattura = idFattura_example;  // string | Identificativo o nome del file della fattura
            var dir = dir_example;  // string | Direzione della fattura (trasmessa o ricevuta) (optional) 
            var body = ;  // List<byte[]> | xml (optional) 

            try
            {
                // Aggiorna il contenuto XML della Fattura Elettronica
                Fattura result = apiInstance.UpdateXml(idFattura, dir, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureApi.UpdateXml: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idFattura** | **string**| Identificativo o nome del file della fattura | 
 **dir** | **string**| Direzione della fattura (trasmessa o ricevuta) | [optional] 
 **body** | **List&lt;byte[]&gt;**| xml | [optional] 

### Return type

[**Fattura**](Fattura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

