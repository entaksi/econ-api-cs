# Entaksi.eCon.Model.Fattura
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**CodiceDestinatario** | **string** | Il codice destinatario della fattura | [optional] 
**DescrizioneDestinatario** | **string** | Ragione sociale del destinatario della fattura | [optional] 
**PartitaIva** | **string** | Partita IVA del destinatario della fattura | [optional] 
**NomeFile** | **string** | Nome del file della fattura | [optional] 
**Tipo** | **string** | Tipo del documento | [optional] 
**Stato** | **string** | Stato della fattura | [optional] 
**Esito** | **string** | Esito della fattura | [optional] 
**Schema** | **string** | Schema della fattura | [optional] 
**Formato** | **string** | Formato della fattura | [optional] 
**DataCaricamento** | **DateTime?** | La data in cui la fattura è stata caricata nel sistema | [optional] 
**DataSpedizione** | **DateTime?** | La data in cui la fattura è stata inviata | [optional] 
**IdAzienda** | **long?** | Identificativo dell&#39;azienda proprietaria della fattura | [optional] 
**IdPdv** | **long?** | Identificativo del pacchetto di versamento in cui è contenuta la fattura | [optional] 
**DataDocumento** | **DateTime?** |  | [optional] 
**Flag** | **bool?** |  | [optional] 
**IdSdi** | **long?** | Identificativo assegnato dallo SDI alla fattura | [optional] 
**Direzione** | **string** | Direzione della fattura (trasmessa o ricevuta) | [optional] 
**Ricevute** | [**List&lt;Ricevuta&gt;**](Ricevuta.md) |  | [optional] 
**DescrizioneMittente** | **string** | Ragione sociale del mittente della fattura | [optional] 
**PartitaIvaMittente** | **string** | Partita IVA del mittente della fattura | [optional] 
**Dati** | [**List&lt;Dato&gt;**](Dato.md) |  | [optional] 
**Numero** | **string** | Numero del documento | [optional] 
**ElementiLotto** | [**List&lt;ElementoLotto&gt;**](ElementoLotto.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

