# Entaksi.eCon.Model.Totali
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**SpazioOccupato** | **long?** | Spazio occupato nel sistema di archiviazione | [optional] 
**Documenti** | **long?** | Numero di unità documentarie nel sistema di archiviazione | [optional] 
**File** | **long?** | Numero di file nel sistema di archiviazione | [optional] 
**DocumentiScarto** | **long?** | Numero di unità documentarie nel sistema di archiviazione destinate allo scarto nei prossimi 6 mesi | [optional] 
**SpazioScarto** | **long?** | Spazio occupato nel sistema di archiviazione destinato allo scarto nei prossimi 6 mesi | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

