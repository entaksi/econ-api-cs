# Entaksi.eCon.Model.Riferimento
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Numero** | **string** | Il numero della fattura a cui fa riferimento la notifica EC | [optional] 
**Anno** | **int?** | L&#39;anno fiscale della fattura a cui fa riferimento la notifica EC | [optional] 
**Posizione** | **int?** | Posizione della fattura nel lotto | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

