# Entaksi.eCon.Model.Pdd
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**Tipo** | **string** | Tipo di richiesta di documenti | [optional] 
**IdAzienda** | **long?** | Azienda che a cui fa riferimento il pacchetto | [optional] 
**Descrizione** | **string** | Descrizione della richiesta eventualmente indicata dall&#39;utente | [optional] 
**Stato** | **string** | Stato del pacchetto di versamento | [optional] 
**Predicati** | [**List&lt;PredicatoPdd&gt;**](PredicatoPdd.md) |  | [optional] 
**NumeroZip** | **int?** | Numero di zip che compongono il pacchetto | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

