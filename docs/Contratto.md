# Entaksi.eCon.Model.Contratto
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**Inizio** | **DateTime?** | Inizio del periodo di validità del contratto | [optional] 
**Fine** | **DateTime?** | Fine del periodo di validità del contratto | [optional] 
**IdRivenditore** | **long?** | Identificativo del rivenditore che ha stipulato il contratto | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

