# Entaksi.eCon.Model.Rivenditore
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**RagioneSociale** | **string** | Ragione sociale del rivenditore | [optional] 
**Nome** | **string** | Nome della persona di riferimento del rivenditore | [optional] 
**Cognome** | **string** | Cognome della persona di riferimento del rivenditore | [optional] 
**Email** | **string** | Indirizzo email del rivenditore | [optional] 
**EmailPec** | **string** | Indirizzo email PEC del rivenditore | [optional] 
**DataInizio** | **DateTime?** | Data di inizio attività del rivenditore | [optional] 
**DataFine** | **DateTime?** | Data di fine attività del rivenditore | [optional] 
**DocumentiElaborati** | **int?** | Numero totale di documenti elaborati | [optional] 
**DimensioneElaborata** | **int?** | Numero di Kbyte di documenti elaborati | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

