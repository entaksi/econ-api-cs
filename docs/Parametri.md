# Entaksi.eCon.Model.Parametri
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_Parametri** | [**List&lt;Parametro&gt;**](Parametro.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

