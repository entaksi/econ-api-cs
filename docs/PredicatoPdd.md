# Entaksi.eCon.Model.PredicatoPdd
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Metadato** | **string** | Nome del metadato | [optional] 
**Valore** | **string** | Valore del metadato | [optional] 
**Operatore** | **string** | Operatore da applicare nella ricerca | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

