# Entaksi.eCon.Api.UtentiApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Companies**](UtentiApi.md#companies) | **GET** /utenti/{chiaveUtente}/aziende | Recupera l&#39;elenco delle aziende associate all&#39;utente
[**List**](UtentiApi.md#list) | **GET** /utenti | Recupera l&#39;elenco degli utenti
[**Read**](UtentiApi.md#read) | **GET** /utenti/{idUtente} | Recupera le informazioni di un utente
[**Update**](UtentiApi.md#update) | **PUT** /utenti/{idUtente} | Aggiorna le informazioni di un utente


<a name="companies"></a>
# **Companies**
> ElencoAziende Companies (string chiaveUtente, int? inizio = null, int? max = null)

Recupera l'elenco delle aziende associate all'utente

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CompaniesExample
    {
        public void main()
        {
            var apiInstance = new UtentiApi();
            var chiaveUtente = chiaveUtente_example;  // string | Identificativo o indirizzo email dell'utente
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Recupera l'elenco delle aziende associate all'utente
                ElencoAziende result = apiInstance.Companies(chiaveUtente, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UtentiApi.Companies: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chiaveUtente** | **string**| Identificativo o indirizzo email dell&#39;utente | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoAziende**](ElencoAziende.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list"></a>
# **List**
> ElencoUtenti List (int? inizio = null, int? max = null)

Recupera l'elenco degli utenti

Note utenti

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListExample
    {
        public void main()
        {
            var apiInstance = new UtentiApi();
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Recupera l'elenco degli utenti
                ElencoUtenti result = apiInstance.List(inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UtentiApi.List: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoUtenti**](ElencoUtenti.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read"></a>
# **Read**
> Utente Read (long? idUtente)

Recupera le informazioni di un utente

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadExample
    {
        public void main()
        {
            var apiInstance = new UtentiApi();
            var idUtente = 789;  // long? | Identificativo dell'utente

            try
            {
                // Recupera le informazioni di un utente
                Utente result = apiInstance.Read(idUtente);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UtentiApi.Read: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idUtente** | **long?**| Identificativo dell&#39;utente | 

### Return type

[**Utente**](Utente.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update"></a>
# **Update**
> Utente Update (long? idUtente, Utente body = null)

Aggiorna le informazioni di un utente

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UpdateExample
    {
        public void main()
        {
            var apiInstance = new UtentiApi();
            var idUtente = 789;  // long? | Identificativo dell'utente
            var body = new Utente(); // Utente | Informazioni aggiornate dell'utente (optional) 

            try
            {
                // Aggiorna le informazioni di un utente
                Utente result = apiInstance.Update(idUtente, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling UtentiApi.Update: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idUtente** | **long?**| Identificativo dell&#39;utente | 
 **body** | [**Utente**](Utente.md)| Informazioni aggiornate dell&#39;utente | [optional] 

### Return type

[**Utente**](Utente.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

