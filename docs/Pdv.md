# Entaksi.eCon.Model.Pdv
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**IdAzienda** | **long?** | Azienda che a cui fa riferimento il pacchetto | [optional] 
**DataCaricamento** | **DateTime?** | Data del caricamento del pacchetto | [optional] 
**DataElaborazione** | **DateTime?** | Data in cui il pacchetto è stato elaborato | [optional] 
**Formato** | **string** | Formato del pacchetto di versamento | [optional] 
**Stato** | **string** | Stato del pacchetto di versamento | [optional] 
**Anno** | **int?** | Anno di riferimento dei documenti contenuti nel pacchetto di versamento | [optional] 
**Registro** | **string** | Registro di archiviazione dei documenti | [optional] 
**Urn** | **string** | Urn del pacchetto di versamento | [optional] 
**NomeFile** | **string** | Nome del file del pacchetto di versamento | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

