# Entaksi.eCon.Model.ElencoStoriciFattura
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Paginazione** | [**Paginazione**](Paginazione.md) |  | [optional] 
**StoriciFattura** | [**List&lt;StoricoFattura&gt;**](StoricoFattura.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

