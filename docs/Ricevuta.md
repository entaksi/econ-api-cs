# Entaksi.eCon.Model.Ricevuta
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**DataRicevuta** | **DateTime?** | Data della ricevuta | [optional] 
**IdFattura** | **long?** | Fattura a cui fa riferimento la notifica | [optional] 
**Flag** | **bool?** | Indica se la notifica è stata letta | [optional] 
**Tipo** | **string** | Tipo di notifica | [optional] 
**NomeFile** | **string** | Nome del file assegnato alla notifica | [optional] 
**Esito** | **string** | Esito indicato nella notifica (solo per le notifice EC e NE) | [optional] 
**Note** | **string** | Note della notifica | [optional] 
**Riferimento** | [**Riferimento**](Riferimento.md) | Riferimento alla fattura all&#39;interno del lotto (solo per le notifiche EC e NE) | [optional] 
**Codice** | **string** | Identificativo del messaggio per il committente (solo per le notifiche EC e NE) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

