# Entaksi.eCon.Api.VersioneApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Get**](VersioneApi.md#get) | **GET** /versione | Restituisce il numero di versione delle API
[**Xsd**](VersioneApi.md#xsd) | **GET** /versione/xsd | Restituisce lo schema XSD che definisce le varie entità utilizzate nelle API


<a name="get"></a>
# **Get**
> void Get ()

Restituisce il numero di versione delle API

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetExample
    {
        public void main()
        {
            var apiInstance = new VersioneApi();

            try
            {
                // Restituisce il numero di versione delle API
                apiInstance.Get();
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VersioneApi.Get: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="xsd"></a>
# **Xsd**
> void Xsd ()

Restituisce lo schema XSD che definisce le varie entità utilizzate nelle API

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class XsdExample
    {
        public void main()
        {
            var apiInstance = new VersioneApi();

            try
            {
                // Restituisce lo schema XSD che definisce le varie entità utilizzate nelle API
                apiInstance.Xsd();
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling VersioneApi.Xsd: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

