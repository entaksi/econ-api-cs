# Entaksi.eCon.Model.Azienda
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**DataRegistrazione** | **DateTime?** | Data di registrazione dell&#39;azienda | [optional] 
**Nome** | **string** | Nome | [optional] 
**Cognome** | **string** | Cognome | [optional] 
**RagioneSociale** | **string** | Ragione sociale | [optional] 
**PartitaIva** | **string** | Partita IVA | [optional] 
**CodiceFiscale** | **string** | Codice fiscale dell&#39;azienda. | [optional] 
**CodiceUnita** | **string** | Codice unità organizzativa | [optional] 
**Email** | **string** | Indirizzo email dell&#39;azienda | [optional] 
**EmailPec** | **string** | Indirizzo email PEC dell&#39;azienda | [optional] 
**Telefono** | **string** | Telefono dell&#39;azienda | [optional] 
**IdContratto** | **long?** | Identificativo del contratto a cui è associata l&#39;azienda | [optional] 
**Indirizzo** | **string** | Indirizzo (via e numero civico) dell&#39;azienda | [optional] 
**Localita** | **string** | Località (comune) dell&#39;azienda | [optional] 
**Paese** | **string** | Paese dell&#39;azienda (codice ISO 3166-1 alpha-2) | [optional] 
**Cap** | **string** | CAP dell&#39;azienda. | [optional] 
**Provincia** | **string** | Provincia dell&#39;azienda (codice a 2 lettere) | [optional] 
**Stato** | **string** | Stato di attivazione dell&#39;azienda al servizio | [optional] 
**CanaleDelega** | **string** | Il canale di ricezione della delega | [optional] 
**DataRicezioneDelega** | **DateTime?** | La data di ricezione della delega | [optional] 
**ErroreDelega** | **string** | L&#39;eventuale errore formale nella delega | [optional] 
**DataInvioDelega** | **DateTime?** | La data di invio della delega controfirmata | [optional] 
**DataFineDelega** | **DateTime?** | La data di fine della delega | [optional] 
**Pa** | **bool?** | Indica se l&#39;azienda è una Pubblica Amministrazione | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

