# Entaksi.eCon.Model.Esito
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Codice** | **string** | Codice dell&#39;esito | [optional] 
**Messaggio** | **string** | Messaggio di stato | [optional] 
**Dettagli** | **List&lt;string&gt;** | Dettagli | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

