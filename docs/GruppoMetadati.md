# Entaksi.eCon.Model.GruppoMetadati
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**Nome** | **string** | Nome del gruppo di metadati. | [optional] 
**Descrizione** | **string** | Descrizione del gruppo di metadati. | [optional] 
**Prefisso** | **string** | Prefisso assegnato al gruppo di metadati. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

