# Entaksi.eCon.Model.ElementoLotto
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**Posizione** | **int?** | Posizione dell&#39;elemento nel lotto di fatture | [optional] 
**Anno** | **int?** | Anno della fattura | [optional] 
**Sezionale** | **string** | Sezionale della fattura | [optional] 
**Numero** | **int?** | Numero della fattura | [optional] 
**Tipo** | **string** | Tipo del documento | [optional] 
**Esito** | **string** | Esito della fattura | [optional] 
**NumeroDocumento** | **string** | Numero del documento | [optional] 
**Importo** | **decimal?** | Importo della fattura | [optional] 
**Valuta** | **string** | Valuta dell&#39;importo della fattura | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

