# Entaksi.eCon.Model.ElencoMetadati
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Metadati** | [**List&lt;Metadato&gt;**](Metadato.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

