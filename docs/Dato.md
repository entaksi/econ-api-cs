# Entaksi.eCon.Model.Dato
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Nome** | **string** | Nome del dato | [optional] 
**Valore** | **string** | Valore del dato | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

