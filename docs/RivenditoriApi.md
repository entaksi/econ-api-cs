# Entaksi.eCon.Api.RivenditoriApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddClient**](RivenditoriApi.md#addclient) | **PUT** /rivenditori/{resellerId}/clients/{clientId} | Associa un client a un rivenditore
[**AddUser**](RivenditoriApi.md#adduser) | **PUT** /rivenditori/{idRivenditore}/utenti/{chiaveUtente}/add | Associa un utente a un rivenditore
[**Companies**](RivenditoriApi.md#companies) | **GET** /rivenditori/{idRivenditore}/aziende | Recupera le aziende visibili dall&#39;utente ed associate al rivenditore
[**Contracts**](RivenditoriApi.md#contracts) | **GET** /rivenditori/{idRivenditore}/contratti | Recupera i contratti associati al rivenditore
[**Create**](RivenditoriApi.md#create) | **POST** /rivenditori | Crea un nuovo rivenditore
[**CreateContract**](RivenditoriApi.md#createcontract) | **POST** /rivenditori/{idRivenditore}/contratti | Crea un nuovo contratto associato al rivenditore
[**CreateContractAndCompany**](RivenditoriApi.md#createcontractandcompany) | **POST** /rivenditori/{idRivenditore}/aziende | Crea una nuova azienda ed un nuovo contratto associato al rivenditore
[**GetClientIds**](RivenditoriApi.md#getclientids) | **GET** /rivenditori/{resellerId}/clients | Recupera l&#39;elenco dei client associati a un rivenditore
[**GetParameters**](RivenditoriApi.md#getparameters) | **GET** /rivenditori/{idRivenditore}/parametri/{nome} | Recupera l&#39;elenco dei parametri di configurazione di un rivenditore
[**GetParameters_0**](RivenditoriApi.md#getparameters_0) | **GET** /rivenditori/{idRivenditore}/parametri | Recupera l&#39;elenco dei parametri di configurazione di un rivenditore
[**GetRoles**](RivenditoriApi.md#getroles) | **GET** /rivenditori/{idRivenditore}/roles | Recupera l&#39;elenco dei  ruoli dell&#39;utente su di un rivenditore
[**List**](RivenditoriApi.md#list) | **GET** /rivenditori | Recupera l&#39;elenco dei rivenditori
[**Read**](RivenditoriApi.md#read) | **GET** /rivenditori/{idRivenditore} | Recupera le informazioni di un rivenditore
[**RemoveClient**](RivenditoriApi.md#removeclient) | **DELETE** /rivenditori/{resellerId}/clients/{clientId} | Dissocia un client da un rivenditore
[**RemoveUser**](RivenditoriApi.md#removeuser) | **PUT** /rivenditori/{idRivenditore}/utenti/{chiaveUtente}/remove | Dissocia un utente da un rivenditore
[**StatusTotalizations**](RivenditoriApi.md#statustotalizations) | **GET** /rivenditori/{idRivenditore}/aziende/totaliStati | Recupera i totali per stati delle aziende associate al rivenditore
[**Totalizations**](RivenditoriApi.md#totalizations) | **GET** /rivenditori/totali | Recupera i totali del sistema
[**Totalizations_0**](RivenditoriApi.md#totalizations_0) | **GET** /rivenditori/{resellerId}/totali | Recupera i totali del rivenditore
[**Update**](RivenditoriApi.md#update) | **PUT** /rivenditori/{idRivenditore} | Aggiorna le informazioni di un rivenditore
[**Users**](RivenditoriApi.md#users) | **GET** /rivenditori/{idRivenditore}/utenti | Recupera l&#39;elenco degli utenti associati al rivenditore


<a name="addclient"></a>
# **AddClient**
> Info AddClient (long? resellerId, string clientId)

Associa un client a un rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class AddClientExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var resellerId = 789;  // long? | Identificativo del rivenditore
            var clientId = clientId_example;  // string | Identificativo dell'utente

            try
            {
                // Associa un client a un rivenditore
                Info result = apiInstance.AddClient(resellerId, clientId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.AddClient: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resellerId** | **long?**| Identificativo del rivenditore | 
 **clientId** | **string**| Identificativo dell&#39;utente | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="adduser"></a>
# **AddUser**
> Info AddUser (long? idRivenditore, string chiaveUtente)

Associa un utente a un rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class AddUserExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Identificativo del rivenditore
            var chiaveUtente = chiaveUtente_example;  // string | Identificativo o indirizzo email dell'utente

            try
            {
                // Associa un utente a un rivenditore
                Info result = apiInstance.AddUser(idRivenditore, chiaveUtente);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.AddUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Identificativo del rivenditore | 
 **chiaveUtente** | **string**| Identificativo o indirizzo email dell&#39;utente | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="companies"></a>
# **Companies**
> ElencoAziende Companies (long? idRivenditore, int? inizio = null, int? max = null, string q = null, string email = null, string emailPec = null, string stato = null, List<string> s = null)

Recupera le aziende visibili dall'utente ed associate al rivenditore

Recupera l'elenco delle aziende visibili dall'utente collegato ed associate al rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CompaniesExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Codice del rivenditore
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var q = q_example;  // string | Filtro per ragione sociale, partita iva e codice fiscale (optional) 
            var email = email_example;  // string | Filtro per email (optional) 
            var emailPec = emailPec_example;  // string | Filtro per pec (optional) 
            var stato = stato_example;  // string | Filtro per stato azienda (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera le aziende visibili dall'utente ed associate al rivenditore
                ElencoAziende result = apiInstance.Companies(idRivenditore, inizio, max, q, email, emailPec, stato, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.Companies: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Codice del rivenditore | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **q** | **string**| Filtro per ragione sociale, partita iva e codice fiscale | [optional] 
 **email** | **string**| Filtro per email | [optional] 
 **emailPec** | **string**| Filtro per pec | [optional] 
 **stato** | **string**| Filtro per stato azienda | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoAziende**](ElencoAziende.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="contracts"></a>
# **Contracts**
> ElencoContratti Contracts (long? idRivenditore, int? inizio = null, int? max = null, List<string> s = null)

Recupera i contratti associati al rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ContractsExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Codice del rivenditore
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera i contratti associati al rivenditore
                ElencoContratti result = apiInstance.Contracts(idRivenditore, inizio, max, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.Contracts: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Codice del rivenditore | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoContratti**](ElencoContratti.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="create"></a>
# **Create**
> Rivenditore Create (Rivenditore body = null)

Crea un nuovo rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var body = new Rivenditore(); // Rivenditore | Informazioni del nuovo rivenditore (optional) 

            try
            {
                // Crea un nuovo rivenditore
                Rivenditore result = apiInstance.Create(body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.Create: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Rivenditore**](Rivenditore.md)| Informazioni del nuovo rivenditore | [optional] 

### Return type

[**Rivenditore**](Rivenditore.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createcontract"></a>
# **CreateContract**
> Contratto CreateContract (long? idRivenditore, Contratto body = null)

Crea un nuovo contratto associato al rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateContractExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Codice del rivenditore
            var body = new Contratto(); // Contratto | Informazioni del nuovo contratto (optional) 

            try
            {
                // Crea un nuovo contratto associato al rivenditore
                Contratto result = apiInstance.CreateContract(idRivenditore, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.CreateContract: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Codice del rivenditore | 
 **body** | [**Contratto**](Contratto.md)| Informazioni del nuovo contratto | [optional] 

### Return type

[**Contratto**](Contratto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createcontractandcompany"></a>
# **CreateContractAndCompany**
> Azienda CreateContractAndCompany (long? idRivenditore, Azienda body = null)

Crea una nuova azienda ed un nuovo contratto associato al rivenditore

Crea una nuova azienda associata ad un nuovo contratto da perfezionare in futuro

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateContractAndCompanyExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Codice del rivenditore
            var body = new Azienda(); // Azienda | Dati dell'azienda da creare (optional) 

            try
            {
                // Crea una nuova azienda ed un nuovo contratto associato al rivenditore
                Azienda result = apiInstance.CreateContractAndCompany(idRivenditore, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.CreateContractAndCompany: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Codice del rivenditore | 
 **body** | [**Azienda**](Azienda.md)| Dati dell&#39;azienda da creare | [optional] 

### Return type

[**Azienda**](Azienda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getclientids"></a>
# **GetClientIds**
> Clients GetClientIds (long? resellerId)

Recupera l'elenco dei client associati a un rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetClientIdsExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var resellerId = 789;  // long? | Identificativo del rivenditore

            try
            {
                // Recupera l'elenco dei client associati a un rivenditore
                Clients result = apiInstance.GetClientIds(resellerId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.GetClientIds: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resellerId** | **long?**| Identificativo del rivenditore | 

### Return type

[**Clients**](Clients.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getparameters"></a>
# **GetParameters**
> Parametro GetParameters (long? idRivenditore, string nome)

Recupera l'elenco dei parametri di configurazione di un rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetParametersExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Identificativo del rivenditore
            var nome = nome_example;  // string | Nome del parametro di configurazione

            try
            {
                // Recupera l'elenco dei parametri di configurazione di un rivenditore
                Parametro result = apiInstance.GetParameters(idRivenditore, nome);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.GetParameters: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Identificativo del rivenditore | 
 **nome** | **string**| Nome del parametro di configurazione | 

### Return type

[**Parametro**](Parametro.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getparameters_0"></a>
# **GetParameters_0**
> Parametri GetParameters_0 (long? idRivenditore)

Recupera l'elenco dei parametri di configurazione di un rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetParameters_0Example
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Identificativo del rivenditore

            try
            {
                // Recupera l'elenco dei parametri di configurazione di un rivenditore
                Parametri result = apiInstance.GetParameters_0(idRivenditore);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.GetParameters_0: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Identificativo del rivenditore | 

### Return type

[**Parametri**](Parametri.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getroles"></a>
# **GetRoles**
> Roles GetRoles (long? idRivenditore)

Recupera l'elenco dei  ruoli dell'utente su di un rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetRolesExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Identificativo del rivenditore

            try
            {
                // Recupera l'elenco dei  ruoli dell'utente su di un rivenditore
                Roles result = apiInstance.GetRoles(idRivenditore);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.GetRoles: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Identificativo del rivenditore | 

### Return type

[**Roles**](Roles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list"></a>
# **List**
> ElencoRivenditori List (int? inizio = null, int? max = null, List<string> s = null)

Recupera l'elenco dei rivenditori

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco dei rivenditori
                ElencoRivenditori result = apiInstance.List(inizio, max, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.List: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoRivenditori**](ElencoRivenditori.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read"></a>
# **Read**
> Rivenditore Read (long? idRivenditore)

Recupera le informazioni di un rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Codice del rivenditore

            try
            {
                // Recupera le informazioni di un rivenditore
                Rivenditore result = apiInstance.Read(idRivenditore);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.Read: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Codice del rivenditore | 

### Return type

[**Rivenditore**](Rivenditore.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removeclient"></a>
# **RemoveClient**
> Info RemoveClient (long? resellerId, string clientId)

Dissocia un client da un rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RemoveClientExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var resellerId = 789;  // long? | Identificativo del rivenditore
            var clientId = clientId_example;  // string | Identificativo dell'utente

            try
            {
                // Dissocia un client da un rivenditore
                Info result = apiInstance.RemoveClient(resellerId, clientId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.RemoveClient: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resellerId** | **long?**| Identificativo del rivenditore | 
 **clientId** | **string**| Identificativo dell&#39;utente | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removeuser"></a>
# **RemoveUser**
> Info RemoveUser (long? idRivenditore, string chiaveUtente)

Dissocia un utente da un rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RemoveUserExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Identificativo del rivenditore
            var chiaveUtente = chiaveUtente_example;  // string | Identificativo o indirizzo email dell'utente

            try
            {
                // Dissocia un utente da un rivenditore
                Info result = apiInstance.RemoveUser(idRivenditore, chiaveUtente);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.RemoveUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Identificativo del rivenditore | 
 **chiaveUtente** | **string**| Identificativo o indirizzo email dell&#39;utente | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="statustotalizations"></a>
# **StatusTotalizations**
> ElencoValori StatusTotalizations (long? idRivenditore)

Recupera i totali per stati delle aziende associate al rivenditore

Recupera i totali per stati delle aziende associate al rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class StatusTotalizationsExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Codice del rivenditore

            try
            {
                // Recupera i totali per stati delle aziende associate al rivenditore
                ElencoValori result = apiInstance.StatusTotalizations(idRivenditore);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.StatusTotalizations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Codice del rivenditore | 

### Return type

[**ElencoValori**](ElencoValori.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="totalizations"></a>
# **Totalizations**
> Totali Totalizations ()

Recupera i totali del sistema

Recupera i totali del sistema.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class TotalizationsExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();

            try
            {
                // Recupera i totali del sistema
                Totali result = apiInstance.Totalizations();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.Totalizations: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Totali**](Totali.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="totalizations_0"></a>
# **Totalizations_0**
> Totali Totalizations_0 (long? resellerId)

Recupera i totali del rivenditore

Recupera i totali del rivenditore.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class Totalizations_0Example
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var resellerId = 789;  // long? | Identificativo del rivenditore

            try
            {
                // Recupera i totali del rivenditore
                Totali result = apiInstance.Totalizations_0(resellerId);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.Totalizations_0: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **resellerId** | **long?**| Identificativo del rivenditore | 

### Return type

[**Totali**](Totali.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update"></a>
# **Update**
> Rivenditore Update (long? idRivenditore, Rivenditore body = null)

Aggiorna le informazioni di un rivenditore

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UpdateExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Codice del rivenditore
            var body = new Rivenditore(); // Rivenditore | Informazioni aggiornate del rivenditore (optional) 

            try
            {
                // Aggiorna le informazioni di un rivenditore
                Rivenditore result = apiInstance.Update(idRivenditore, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.Update: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Codice del rivenditore | 
 **body** | [**Rivenditore**](Rivenditore.md)| Informazioni aggiornate del rivenditore | [optional] 

### Return type

[**Rivenditore**](Rivenditore.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="users"></a>
# **Users**
> ElencoUtenti Users (long? idRivenditore, int? inizio = null, int? max = null)

Recupera l'elenco degli utenti associati al rivenditore

Recupera l'elenco degli utenti associati al rivenditore.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UsersExample
    {
        public void main()
        {
            var apiInstance = new RivenditoriApi();
            var idRivenditore = 789;  // long? | Identificativo del rivenditore
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Recupera l'elenco degli utenti associati al rivenditore
                ElencoUtenti result = apiInstance.Users(idRivenditore, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RivenditoriApi.Users: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRivenditore** | **long?**| Identificativo del rivenditore | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoUtenti**](ElencoUtenti.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

