# Entaksi.eCon.Api.RicevuteApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateFromXml**](RicevuteApi.md#createfromxml) | **POST** /ricevute/xml | Crea una nuova ricevuta EC caricando il contenuto XML
[**Flag**](RicevuteApi.md#flag) | **POST** /ricevute/{idRicevuta}/flag | Imposta il flag sulla ricevuta
[**List**](RicevuteApi.md#list) | **GET** /ricevute | Recupere l&#39;elenco delle ricevute
[**Read**](RicevuteApi.md#read) | **GET** /ricevute/{idRicevuta} | Recupera le informazioni di una ricevuta
[**ReadXml**](RicevuteApi.md#readxml) | **GET** /ricevute/{idRicevuta}/xml | Recupera il contenuto XML di una ricevuta
[**ReadZip**](RicevuteApi.md#readzip) | **GET** /ricevute/{idRicevuta}/zip | Recupera il contenuto ZIP di una ricevuta (valido solo per le ricevute di tipo AT
[**Unflag**](RicevuteApi.md#unflag) | **POST** /ricevute/{idRicevuta}/unflag | Rimuove il flag sulla ricevuta


<a name="createfromxml"></a>
# **CreateFromXml**
> Ricevuta CreateFromXml (List<byte[]> body = null)

Crea una nuova ricevuta EC caricando il contenuto XML

Il servizio accetta ricevute EC nel formato definito dallo SDI ricavando la fattura dai dati indicati nella ricevuta.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateFromXmlExample
    {
        public void main()
        {
            var apiInstance = new RicevuteApi();
            var body = ;  // List<byte[]> | xml (optional) 

            try
            {
                // Crea una nuova ricevuta EC caricando il contenuto XML
                Ricevuta result = apiInstance.CreateFromXml(body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RicevuteApi.CreateFromXml: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **List&lt;byte[]&gt;**| xml | [optional] 

### Return type

[**Ricevuta**](Ricevuta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="flag"></a>
# **Flag**
> Info Flag (string idRicevuta)

Imposta il flag sulla ricevuta

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class FlagExample
    {
        public void main()
        {
            var apiInstance = new RicevuteApi();
            var idRicevuta = idRicevuta_example;  // string | Codice della ricevuta

            try
            {
                // Imposta il flag sulla ricevuta
                Info result = apiInstance.Flag(idRicevuta);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RicevuteApi.Flag: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRicevuta** | **string**| Codice della ricevuta | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list"></a>
# **List**
> ElencoRicevute List (int? inizio = null, int? max = null, string dataInizio = null, string dataFine = null, string tipo = null, string flag = null, List<string> s = null)

Recupere l'elenco delle ricevute

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListExample
    {
        public void main()
        {
            var apiInstance = new RicevuteApi();
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var dataInizio = dataInizio_example;  // string | Data inizio selezione ISO-8601 (optional) 
            var dataFine = dataFine_example;  // string | Data fine selezione ISO-8601 (optional) 
            var tipo = tipo_example;  // string | Tipo ricevuta (optional) 
            var flag = flag_example;  // string | Flag lettura (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupere l'elenco delle ricevute
                ElencoRicevute result = apiInstance.List(inizio, max, dataInizio, dataFine, tipo, flag, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RicevuteApi.List: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **dataInizio** | **string**| Data inizio selezione ISO-8601 | [optional] 
 **dataFine** | **string**| Data fine selezione ISO-8601 | [optional] 
 **tipo** | **string**| Tipo ricevuta | [optional] 
 **flag** | **string**| Flag lettura | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoRicevute**](ElencoRicevute.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read"></a>
# **Read**
> Ricevuta Read (string idRicevuta)

Recupera le informazioni di una ricevuta

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadExample
    {
        public void main()
        {
            var apiInstance = new RicevuteApi();
            var idRicevuta = idRicevuta_example;  // string | Codice della ricevuta

            try
            {
                // Recupera le informazioni di una ricevuta
                Ricevuta result = apiInstance.Read(idRicevuta);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RicevuteApi.Read: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRicevuta** | **string**| Codice della ricevuta | 

### Return type

[**Ricevuta**](Ricevuta.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="readxml"></a>
# **ReadXml**
> string ReadXml (string idRicevuta)

Recupera il contenuto XML di una ricevuta

Recupera il contenuto XML della Ricevuta

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadXmlExample
    {
        public void main()
        {
            var apiInstance = new RicevuteApi();
            var idRicevuta = idRicevuta_example;  // string | Identificativo della ricevuta

            try
            {
                // Recupera il contenuto XML di una ricevuta
                string result = apiInstance.ReadXml(idRicevuta);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RicevuteApi.ReadXml: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRicevuta** | **string**| Identificativo della ricevuta | 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="readzip"></a>
# **ReadZip**
> System.IO.Stream ReadZip (string idRicevuta)

Recupera il contenuto ZIP di una ricevuta (valido solo per le ricevute di tipo AT

Recupera il contenuto ZIP della Ricevuta

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadZipExample
    {
        public void main()
        {
            var apiInstance = new RicevuteApi();
            var idRicevuta = idRicevuta_example;  // string | Identificativo della ricevuta

            try
            {
                // Recupera il contenuto ZIP di una ricevuta (valido solo per le ricevute di tipo AT
                System.IO.Stream result = apiInstance.ReadZip(idRicevuta);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RicevuteApi.ReadZip: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRicevuta** | **string**| Identificativo della ricevuta | 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json, application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="unflag"></a>
# **Unflag**
> Info Unflag (string idRicevuta)

Rimuove il flag sulla ricevuta

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UnflagExample
    {
        public void main()
        {
            var apiInstance = new RicevuteApi();
            var idRicevuta = idRicevuta_example;  // string | Codice della ricevuta

            try
            {
                // Rimuove il flag sulla ricevuta
                Info result = apiInstance.Unflag(idRicevuta);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling RicevuteApi.Unflag: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idRicevuta** | **string**| Codice della ricevuta | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

