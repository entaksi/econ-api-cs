# Entaksi.eCon.Model.ElencoStoriciAzienda
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Paginazione** | [**Paginazione**](Paginazione.md) |  | [optional] 
**StoriciAzienda** | [**List&lt;StoricoAzienda&gt;**](StoricoAzienda.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

