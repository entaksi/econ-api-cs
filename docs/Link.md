# Entaksi.eCon.Model.Link
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Rel** | **string** |  | [optional] 
**Href** | **string** |  | [optional] 
**Title** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

