# Entaksi.eCon.Api.FattureSingoleApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddNamedValue**](FattureSingoleApi.md#addnamedvalue) | **POST** /fattureSingole/{idElemento}/dati | Aggiunge un valore associato alla fattura.
[**ChangeNamedValue**](FattureSingoleApi.md#changenamedvalue) | **PUT** /fattureSingole/{idElemento}/dati/{nome} | Modifica un valore associato alla fattura.
[**Csv**](FattureSingoleApi.md#csv) | **GET** /fattureSingole.csv | Recupera l&#39;elenco delle fatture singole visibili dall&#39;utente in formato CSV
[**DeleteNamedValue**](FattureSingoleApi.md#deletenamedvalue) | **DELETE** /fattureSingole/{idElemento}/dati/{nome} | Elimina un valore associato alla fattura.
[**GetNamedValue**](FattureSingoleApi.md#getnamedvalue) | **GET** /fattureSingole/{idElemento}/dati/{nome} | Recupera un valore associato alla fattura.
[**InvoiceItemById**](FattureSingoleApi.md#invoiceitembyid) | **GET** /fattureSingole/{idElemento} | Recupera le informazioni sulla fattura singola.
[**ListNamedValues**](FattureSingoleApi.md#listnamedvalues) | **GET** /fattureSingole/{idElemento}/dati | Recupera i dati associati alla fattura.
[**SingleInvoices**](FattureSingoleApi.md#singleinvoices) | **GET** /fattureSingole | Recupera l&#39;elenco delle fatture singole visibili dall&#39;utente


<a name="addnamedvalue"></a>
# **AddNamedValue**
> Dato AddNamedValue (long? idElemento, Dato body = null)

Aggiunge un valore associato alla fattura.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class AddNamedValueExample
    {
        public void main()
        {
            var apiInstance = new FattureSingoleApi();
            var idElemento = 789;  // long? | Identificativo della fattura singola.
            var body = new Dato(); // Dato | Dato da aggiungere alla fattura (optional) 

            try
            {
                // Aggiunge un valore associato alla fattura.
                Dato result = apiInstance.AddNamedValue(idElemento, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureSingoleApi.AddNamedValue: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idElemento** | **long?**| Identificativo della fattura singola. | 
 **body** | [**Dato**](Dato.md)| Dato da aggiungere alla fattura | [optional] 

### Return type

[**Dato**](Dato.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="changenamedvalue"></a>
# **ChangeNamedValue**
> Dato ChangeNamedValue (long? idElemento, string nome, Dato body = null)

Modifica un valore associato alla fattura.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ChangeNamedValueExample
    {
        public void main()
        {
            var apiInstance = new FattureSingoleApi();
            var idElemento = 789;  // long? | Identificativo della fattura singola.
            var nome = nome_example;  // string | Nome del dato da modificare.
            var body = new Dato(); // Dato | Dato da modificare sulla fattura (optional) 

            try
            {
                // Modifica un valore associato alla fattura.
                Dato result = apiInstance.ChangeNamedValue(idElemento, nome, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureSingoleApi.ChangeNamedValue: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idElemento** | **long?**| Identificativo della fattura singola. | 
 **nome** | **string**| Nome del dato da modificare. | 
 **body** | [**Dato**](Dato.md)| Dato da modificare sulla fattura | [optional] 

### Return type

[**Dato**](Dato.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="csv"></a>
# **Csv**
> System.IO.Stream Csv (string anno = null, string dataInizio = null, string dataFine = null, string numeroDocumento = null, string numero = null, string sezionale = null, string nomeFile = null, string destinatario = null, string mittente = null, string importo = null, string q = null, string dir = null, string status = null, string esito = null, string schema = null, string flag = null, List<string> s = null, List<string> d = null)

Recupera l'elenco delle fatture singole visibili dall'utente in formato CSV

Restituisce l'elenco delle fatture singole visibili dall'utente in formato CSV.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CsvExample
    {
        public void main()
        {
            var apiInstance = new FattureSingoleApi();
            var anno = anno_example;  // string | Ricerca per anno (optional) 
            var dataInizio = dataInizio_example;  // string | Data documento inizio (yyyy-mm-dd) (optional) 
            var dataFine = dataFine_example;  // string | Data documento fine (yyyy-mm-dd) (optional) 
            var numeroDocumento = numeroDocumento_example;  // string | Ricerca per numero documento (optional) 
            var numero = numero_example;  // string | Ricerca per numero fattura (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale fattura (optional) 
            var nomeFile = nomeFile_example;  // string | Ricerca per nome file (optional) 
            var destinatario = destinatario_example;  // string | Ricerca per destinatario (optional) 
            var mittente = mittente_example;  // string | Ricerca per mittente (optional) 
            var importo = importo_example;  // string | Ricerca per importo (optional) 
            var q = q_example;  // string | Ricerca a testo libero (optional) 
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 
            var status = status_example;  // string | Stato fattura (optional) 
            var esito = esito_example;  // string | Esito fattura (optional) 
            var schema = schema_example;  // string | Schema fattura (optional) 
            var flag = flag_example;  // string | Flag modificato (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 
            var d = new List<string>(); // List<string> | Dato aggiuntivo (optional) 

            try
            {
                // Recupera l'elenco delle fatture singole visibili dall'utente in formato CSV
                System.IO.Stream result = apiInstance.Csv(anno, dataInizio, dataFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, q, dir, status, esito, schema, flag, s, d);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureSingoleApi.Csv: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **anno** | **string**| Ricerca per anno | [optional] 
 **dataInizio** | **string**| Data documento inizio (yyyy-mm-dd) | [optional] 
 **dataFine** | **string**| Data documento fine (yyyy-mm-dd) | [optional] 
 **numeroDocumento** | **string**| Ricerca per numero documento | [optional] 
 **numero** | **string**| Ricerca per numero fattura | [optional] 
 **sezionale** | **string**| Ricerca per sezionale fattura | [optional] 
 **nomeFile** | **string**| Ricerca per nome file | [optional] 
 **destinatario** | **string**| Ricerca per destinatario | [optional] 
 **mittente** | **string**| Ricerca per mittente | [optional] 
 **importo** | **string**| Ricerca per importo | [optional] 
 **q** | **string**| Ricerca a testo libero | [optional] 
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 
 **status** | **string**| Stato fattura | [optional] 
 **esito** | **string**| Esito fattura | [optional] 
 **schema** | **string**| Schema fattura | [optional] 
 **flag** | **string**| Flag modificato | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 
 **d** | [**List&lt;string&gt;**](string.md)| Dato aggiuntivo | [optional] 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/csv, application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="deletenamedvalue"></a>
# **DeleteNamedValue**
> Info DeleteNamedValue (long? idElemento, string nome)

Elimina un valore associato alla fattura.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class DeleteNamedValueExample
    {
        public void main()
        {
            var apiInstance = new FattureSingoleApi();
            var idElemento = 789;  // long? | Identificativo della fattura singola.
            var nome = nome_example;  // string | Nome del dato da eliminare.

            try
            {
                // Elimina un valore associato alla fattura.
                Info result = apiInstance.DeleteNamedValue(idElemento, nome);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureSingoleApi.DeleteNamedValue: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idElemento** | **long?**| Identificativo della fattura singola. | 
 **nome** | **string**| Nome del dato da eliminare. | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getnamedvalue"></a>
# **GetNamedValue**
> Dato GetNamedValue (long? idElemento, string nome)

Recupera un valore associato alla fattura.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetNamedValueExample
    {
        public void main()
        {
            var apiInstance = new FattureSingoleApi();
            var idElemento = 789;  // long? | Identificativo della fattura singola.
            var nome = nome_example;  // string | Nome del dato

            try
            {
                // Recupera un valore associato alla fattura.
                Dato result = apiInstance.GetNamedValue(idElemento, nome);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureSingoleApi.GetNamedValue: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idElemento** | **long?**| Identificativo della fattura singola. | 
 **nome** | **string**| Nome del dato | 

### Return type

[**Dato**](Dato.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="invoiceitembyid"></a>
# **InvoiceItemById**
> ElementoLotto InvoiceItemById (long? idElemento)

Recupera le informazioni sulla fattura singola.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class InvoiceItemByIdExample
    {
        public void main()
        {
            var apiInstance = new FattureSingoleApi();
            var idElemento = 789;  // long? | Identificativo della fattura singola.

            try
            {
                // Recupera le informazioni sulla fattura singola.
                ElementoLotto result = apiInstance.InvoiceItemById(idElemento);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureSingoleApi.InvoiceItemById: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idElemento** | **long?**| Identificativo della fattura singola. | 

### Return type

[**ElementoLotto**](ElementoLotto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listnamedvalues"></a>
# **ListNamedValues**
> ElencoDati ListNamedValues (long? idElemento, int? inizio = null, int? max = null)

Recupera i dati associati alla fattura.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListNamedValuesExample
    {
        public void main()
        {
            var apiInstance = new FattureSingoleApi();
            var idElemento = 789;  // long? | Identificativo della fattura singola.
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Recupera i dati associati alla fattura.
                ElencoDati result = apiInstance.ListNamedValues(idElemento, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureSingoleApi.ListNamedValues: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idElemento** | **long?**| Identificativo della fattura singola. | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoDati**](ElencoDati.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="singleinvoices"></a>
# **SingleInvoices**
> ElencoFattureSingole SingleInvoices (int? inizio = null, int? max = null, string dataInizio = null, string dataFine = null, string dataIrInizio = null, string dataIrFine = null, string numeroDocumento = null, string numero = null, string sezionale = null, string nomeFile = null, string destinatario = null, string mittente = null, string importo = null, string q = null, string dir = null, string status = null, string esito = null, string schema = null, string flag = null, List<string> i = null, List<string> s = null, List<string> d = null)

Recupera l'elenco delle fatture singole visibili dall'utente

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class SingleInvoicesExample
    {
        public void main()
        {
            var apiInstance = new FattureSingoleApi();
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var dataInizio = dataInizio_example;  // string | Data documento inizio (yyyy-mm-dd) (optional) 
            var dataFine = dataFine_example;  // string | Data documento fine (yyyy-mm-dd) (optional) 
            var dataIrInizio = dataIrInizio_example;  // string | Data invio/ricezione inizio (yyyy-mm-dd) (optional) 
            var dataIrFine = dataIrFine_example;  // string | Data invio/ricezione fine (yyyy-mm-dd) (optional) 
            var numeroDocumento = numeroDocumento_example;  // string | Ricerca per numero documento (optional) 
            var numero = numero_example;  // string | Ricerca per numero fattura (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale fattura (optional) 
            var nomeFile = nomeFile_example;  // string | Ricerca per nome file (optional) 
            var destinatario = destinatario_example;  // string | Ricerca per destinatario (optional) 
            var mittente = mittente_example;  // string | Ricerca per mittente (optional) 
            var importo = importo_example;  // string | Ricerca per importo (optional) 
            var q = q_example;  // string | Ricerca a testo libero (optional) 
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 
            var status = status_example;  // string | Stato fattura (optional) 
            var esito = esito_example;  // string | Esito fattura (optional) 
            var schema = schema_example;  // string | Schema fattura (optional) 
            var flag = flag_example;  // string | Flag modificato (optional) 
            var i = i_example;  // List<string> | Campi supplementari da includere (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 
            var d = new List<string>(); // List<string> | Dato aggiuntivo (optional) 

            try
            {
                // Recupera l'elenco delle fatture singole visibili dall'utente
                ElencoFattureSingole result = apiInstance.SingleInvoices(inizio, max, dataInizio, dataFine, dataIrInizio, dataIrFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, q, dir, status, esito, schema, flag, i, s, d);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling FattureSingoleApi.SingleInvoices: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **dataInizio** | **string**| Data documento inizio (yyyy-mm-dd) | [optional] 
 **dataFine** | **string**| Data documento fine (yyyy-mm-dd) | [optional] 
 **dataIrInizio** | **string**| Data invio/ricezione inizio (yyyy-mm-dd) | [optional] 
 **dataIrFine** | **string**| Data invio/ricezione fine (yyyy-mm-dd) | [optional] 
 **numeroDocumento** | **string**| Ricerca per numero documento | [optional] 
 **numero** | **string**| Ricerca per numero fattura | [optional] 
 **sezionale** | **string**| Ricerca per sezionale fattura | [optional] 
 **nomeFile** | **string**| Ricerca per nome file | [optional] 
 **destinatario** | **string**| Ricerca per destinatario | [optional] 
 **mittente** | **string**| Ricerca per mittente | [optional] 
 **importo** | **string**| Ricerca per importo | [optional] 
 **q** | **string**| Ricerca a testo libero | [optional] 
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 
 **status** | **string**| Stato fattura | [optional] 
 **esito** | **string**| Esito fattura | [optional] 
 **schema** | **string**| Schema fattura | [optional] 
 **flag** | **string**| Flag modificato | [optional] 
 **i** | **List&lt;string&gt;**| Campi supplementari da includere | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 
 **d** | [**List&lt;string&gt;**](string.md)| Dato aggiuntivo | [optional] 

### Return type

[**ElencoFattureSingole**](ElencoFattureSingole.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

