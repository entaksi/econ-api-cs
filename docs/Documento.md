# Entaksi.eCon.Model.Documento
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | **long?** | Identificativo dell&#39;elemento archivistico. | [optional] 
**Files** | **List&lt;System.IO.Stream&gt;** |  | [optional] 
**Metadati** | [**List&lt;Metadato&gt;**](Metadato.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

