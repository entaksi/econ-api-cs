# Entaksi.eCon.Model.FilePdv
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Links** | [**List&lt;Link&gt;**](Link.md) |  | [optional] 
**Id** | **long?** | Identificativo dell&#39;entità | [optional] 
**IdPdv** | **long?** | Pacchetto di versamento a cui fa riferimento il file | [optional] 
**NomeFile** | **string** | Nome del file allegato al pacchetto di versamento | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

