# Entaksi.eCon.Api.ContrattiApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**AddTag**](ContrattiApi.md#addtag) | **POST** /contratti/{idContratto}/tag | Aggiunge un&#39;etichetta al contratto
[**AddUser**](ContrattiApi.md#adduser) | **PUT** /contratti/{idContratto}/utenti/{chiaveUtente}/add | Associa un utente al contratto
[**Companies**](ContrattiApi.md#companies) | **GET** /contratti/{idContratto}/aziende | Recupera l&#39;elenco delle aziende associate al contratto
[**Flag**](ContrattiApi.md#flag) | **POST** /contratti/{idContratto}/tag/{tag}/flag | Imposta il flag sull&#39;etichetta
[**GetParameters**](ContrattiApi.md#getparameters) | **GET** /contratti/{idContratto}/parametri | Recupera l&#39;elenco dei parametri di configurazione di un contratto
[**GetParameters_0**](ContrattiApi.md#getparameters_0) | **GET** /contratti/{idContratto}/parametri/{nome} | Recupera l&#39;elenco dei parametri di configurazione di un contratto
[**GetRoles**](ContrattiApi.md#getroles) | **GET** /contratti/{idContratto}/roles | Recupera l&#39;elenco dei ruoli dell&#39;utente su di un contratto
[**List**](ContrattiApi.md#list) | **GET** /contratti | Recupera l&#39;elenco dei contratti
[**NewCompany**](ContrattiApi.md#newcompany) | **POST** /contratti/{idContratto}/aziende | Crea una nuova azienda associata al contratto
[**Read**](ContrattiApi.md#read) | **GET** /contratti/{idContratto} | Recupera un contratto
[**RemoveTag**](ContrattiApi.md#removetag) | **DELETE** /contratti/{idContratto}/tag | Rimuove un&#39;etichetta dal contratto
[**RemoveUser**](ContrattiApi.md#removeuser) | **PUT** /contratti/{idContratto}/utenti/{chiaveUtente}/remove | Rimuove un utente dal contratto
[**Tags**](ContrattiApi.md#tags) | **GET** /contratti/{idContratto}/tag | Recupera l&#39;elenco delle etichette di un contratto
[**Totalizations**](ContrattiApi.md#totalizations) | **GET** /contratti/{idContratto}/totali | Recupera i totali del contratto
[**Update**](ContrattiApi.md#update) | **PUT** /contratti/{idContratto} | Aggiorna i dati di un contratto
[**Users**](ContrattiApi.md#users) | **GET** /contratti/{idContratto}/utenti | Recupera l&#39;elenco degli utenti associati al contratto


<a name="addtag"></a>
# **AddTag**
> Info AddTag (long? idContratto, string body = null)

Aggiunge un'etichetta al contratto

Aggiunge un'etichetta al contratto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class AddTagExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto
            var body = body_example;  // string | Etichetta da aggiungere (optional) 

            try
            {
                // Aggiunge un'etichetta al contratto
                Info result = apiInstance.AddTag(idContratto, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.AddTag: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 
 **body** | **string**| Etichetta da aggiungere | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="adduser"></a>
# **AddUser**
> Info AddUser (long? idContratto, string chiaveUtente)

Associa un utente al contratto

Associa un utente al contratto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class AddUserExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto
            var chiaveUtente = chiaveUtente_example;  // string | Identificativo o indirizzo email dell'utente

            try
            {
                // Associa un utente al contratto
                Info result = apiInstance.AddUser(idContratto, chiaveUtente);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.AddUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 
 **chiaveUtente** | **string**| Identificativo o indirizzo email dell&#39;utente | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="companies"></a>
# **Companies**
> ElencoAziende Companies (long? idContratto, int? inizio = null, int? max = null)

Recupera l'elenco delle aziende associate al contratto

Recupera l'elenco delle aziende associate al contratto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CompaniesExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Recupera l'elenco delle aziende associate al contratto
                ElencoAziende result = apiInstance.Companies(idContratto, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.Companies: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoAziende**](ElencoAziende.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="flag"></a>
# **Flag**
> Info Flag (long? idContratto, string tag)

Imposta il flag sull'etichetta

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class FlagExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto
            var tag = tag_example;  // string | Etichetta da flaggare

            try
            {
                // Imposta il flag sull'etichetta
                Info result = apiInstance.Flag(idContratto, tag);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.Flag: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 
 **tag** | **string**| Etichetta da flaggare | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getparameters"></a>
# **GetParameters**
> Parametri GetParameters (long? idContratto)

Recupera l'elenco dei parametri di configurazione di un contratto

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetParametersExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto

            try
            {
                // Recupera l'elenco dei parametri di configurazione di un contratto
                Parametri result = apiInstance.GetParameters(idContratto);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.GetParameters: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 

### Return type

[**Parametri**](Parametri.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getparameters_0"></a>
# **GetParameters_0**
> Parametro GetParameters_0 (long? idContratto, string nome)

Recupera l'elenco dei parametri di configurazione di un contratto

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetParameters_0Example
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto
            var nome = nome_example;  // string | Nome del parametro di configurazione

            try
            {
                // Recupera l'elenco dei parametri di configurazione di un contratto
                Parametro result = apiInstance.GetParameters_0(idContratto, nome);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.GetParameters_0: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 
 **nome** | **string**| Nome del parametro di configurazione | 

### Return type

[**Parametro**](Parametro.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getroles"></a>
# **GetRoles**
> Roles GetRoles (long? idContratto)

Recupera l'elenco dei ruoli dell'utente su di un contratto

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetRolesExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto

            try
            {
                // Recupera l'elenco dei ruoli dell'utente su di un contratto
                Roles result = apiInstance.GetRoles(idContratto);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.GetRoles: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 

### Return type

[**Roles**](Roles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list"></a>
# **List**
> ElencoContratti List (int? inizio = null, int? max = null, List<string> s = null)

Recupera l'elenco dei contratti

Recupera l'elenco dei contratti visibili all'utente collegato.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco dei contratti
                ElencoContratti result = apiInstance.List(inizio, max, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.List: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoContratti**](ElencoContratti.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="newcompany"></a>
# **NewCompany**
> Azienda NewCompany (long? idContratto, Azienda body = null)

Crea una nuova azienda associata al contratto

Crea una nuova azienda associata al contratto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class NewCompanyExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto
            var body = new Azienda(); // Azienda | Informazioni sulla nuova azienda (optional) 

            try
            {
                // Crea una nuova azienda associata al contratto
                Azienda result = apiInstance.NewCompany(idContratto, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.NewCompany: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 
 **body** | [**Azienda**](Azienda.md)| Informazioni sulla nuova azienda | [optional] 

### Return type

[**Azienda**](Azienda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read"></a>
# **Read**
> Contratto Read (long? idContratto)

Recupera un contratto

Recupera i dettagli di un contratto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto

            try
            {
                // Recupera un contratto
                Contratto result = apiInstance.Read(idContratto);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.Read: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 

### Return type

[**Contratto**](Contratto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removetag"></a>
# **RemoveTag**
> Info RemoveTag (long? idContratto, string body = null)

Rimuove un'etichetta dal contratto

Rimuove un'etichetta dal contratto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RemoveTagExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto
            var body = body_example;  // string | Etichetta da rimuovere (optional) 

            try
            {
                // Rimuove un'etichetta dal contratto
                Info result = apiInstance.RemoveTag(idContratto, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.RemoveTag: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 
 **body** | **string**| Etichetta da rimuovere | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removeuser"></a>
# **RemoveUser**
> Info RemoveUser (long? idContratto, string chiaveUtente)

Rimuove un utente dal contratto

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RemoveUserExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto
            var chiaveUtente = chiaveUtente_example;  // string | Identificativo o indirizzo email dell'utente

            try
            {
                // Rimuove un utente dal contratto
                Info result = apiInstance.RemoveUser(idContratto, chiaveUtente);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.RemoveUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 
 **chiaveUtente** | **string**| Identificativo o indirizzo email dell&#39;utente | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: *
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="tags"></a>
# **Tags**
> Etichette Tags (long? idContratto)

Recupera l'elenco delle etichette di un contratto

Recupera l'elenco delle etichette di un contratto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class TagsExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto

            try
            {
                // Recupera l'elenco delle etichette di un contratto
                Etichette result = apiInstance.Tags(idContratto);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.Tags: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 

### Return type

[**Etichette**](Etichette.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="totalizations"></a>
# **Totalizations**
> Totali Totalizations (long? idContratto)

Recupera i totali del contratto

Recupera i totali del contratto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class TotalizationsExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto

            try
            {
                // Recupera i totali del contratto
                Totali result = apiInstance.Totalizations(idContratto);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.Totalizations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 

### Return type

[**Totali**](Totali.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update"></a>
# **Update**
> Contratto Update (long? idContratto, Contratto body = null)

Aggiorna i dati di un contratto

Aggiorna i dati di un contratto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UpdateExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto
            var body = new Contratto(); // Contratto | Informazioni aggiornate sul contratto (optional) 

            try
            {
                // Aggiorna i dati di un contratto
                Contratto result = apiInstance.Update(idContratto, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.Update: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 
 **body** | [**Contratto**](Contratto.md)| Informazioni aggiornate sul contratto | [optional] 

### Return type

[**Contratto**](Contratto.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="users"></a>
# **Users**
> ElencoUtenti Users (long? idContratto, int? inizio = null, int? max = null)

Recupera l'elenco degli utenti associati al contratto

Recupera l'elenco degli utenti associati al contratto.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UsersExample
    {
        public void main()
        {
            var apiInstance = new ContrattiApi();
            var idContratto = 789;  // long? | Codice del contratto
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Recupera l'elenco degli utenti associati al contratto
                ElencoUtenti result = apiInstance.Users(idContratto, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling ContrattiApi.Users: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idContratto** | **long?**| Codice del contratto | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoUtenti**](ElencoUtenti.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

