# Entaksi.eCon.Model.Errore
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Esito** | [**Esito**](Esito.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

