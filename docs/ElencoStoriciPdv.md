# Entaksi.eCon.Model.ElencoStoriciPdv
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Paginazione** | [**Paginazione**](Paginazione.md) |  | [optional] 
**StoriciPdv** | [**List&lt;StoricoPdv&gt;**](StoricoPdv.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

