# Entaksi.eCon.Api.AziendeApi

All URIs are relative to *https://localhost/api/edoc/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**Activate**](AziendeApi.md#activate) | **POST** /aziende/{idAzienda}/attiva | Porta l&#39;azienda nello stato attivo
[**AddParameter**](AziendeApi.md#addparameter) | **POST** /aziende/{idAzienda}/parametri/{nome} | Aggiunge un parametro all&#39;azienda
[**AddTag**](AziendeApi.md#addtag) | **POST** /aziende/{idAzienda}/tag | Aggiunge un&#39;etichetta all&#39;azienda
[**AddUser**](AziendeApi.md#adduser) | **PUT** /aziende/{chiaveAzienda}/utenti/{chiaveUtente}/add | Associa un utente all&#39;azienda
[**CreateCompany**](AziendeApi.md#createcompany) | **POST** /aziende | Crea una nuova azienda
[**CreateFromFile**](AziendeApi.md#createfromfile) | **POST** /aziende/{idAzienda}/fatture/uploadXml | Crea una nuova fattura caricando il file XML
[**CreateInvoice**](AziendeApi.md#createinvoice) | **POST** /aziende/{idAzienda}/fatture | Crea una nuova fattura
[**CreateInvoiceFromXml**](AziendeApi.md#createinvoicefromxml) | **POST** /aziende/{idAzienda}/fatture/xml | Crea una nuova fattura per l&#39;azienda caricando il file XML
[**CreatePdd**](AziendeApi.md#createpdd) | **POST** /aziende/{idAzienda}/pdd | Crea una nuova richiesta di pacchetto di distribuzione per l&#39;azienda
[**CreatePdv**](AziendeApi.md#createpdv) | **POST** /aziende/{idAzienda}/fatture/pdv | Crea un nuovo pacchetto di versamento di Fatture Elettroniche per l&#39;azienda caricando il file ZIP
[**CreatePdvFromFile**](AziendeApi.md#createpdvfromfile) | **POST** /aziende/{idAzienda}/pdv/{pdvFormat} | Crea un nuovo pacchetto di versamento in uno specifico formato caricando il file ZIP con il contenuto
[**CreatePdv_0**](AziendeApi.md#createpdv_0) | **POST** /aziende/{idAzienda}/pdv | Crea un nuovo pacchetto di versamento per l&#39;azienda
[**CreateReceivedInvoices**](AziendeApi.md#createreceivedinvoices) | **POST** /aziende/{idAzienda}/fattureRicevute | Invia le fatture ricevute sul sito AdE caricando i file xml o p7m
[**Error**](AziendeApi.md#error) | **POST** /aziende/{idAzienda}/errore | Porta l&#39;azienda nello stato delega errata
[**GetParameters**](AziendeApi.md#getparameters) | **GET** /aziende/{idAzienda}/parametri/{nome} | Recupera l&#39;elenco dei parametri di configurazione di un&#39;azienda
[**GetParameters_0**](AziendeApi.md#getparameters_0) | **GET** /aziende/{idAzienda}/parametri | Recupera l&#39;elenco dei parametri di configurazione di un&#39;azienda
[**GetRoles**](AziendeApi.md#getroles) | **GET** /aziende/{idAzienda}/roles | Recupera l&#39;elenco dei ruoli dell&#39;utente su di un&#39;azienda
[**History**](AziendeApi.md#history) | **GET** /aziende/{idAzienda}/storico | Storico dell&#39;azienda
[**InvoiceOutcomeTotalizations**](AziendeApi.md#invoiceoutcometotalizations) | **GET** /aziende/{idAzienda}/fatture/totaliEsiti | Recupera i totali per esiti delle fatture dell&#39;azienda
[**InvoiceStatusTotalizations**](AziendeApi.md#invoicestatustotalizations) | **GET** /aziende/{idAzienda}/fatture/totaliStati | Recupera i totali per stati delle fatture dell&#39;azienda
[**Invoices**](AziendeApi.md#invoices) | **GET** /aziende/{idAzienda}/fatture | Recupera l&#39;elenco delle fatture dell&#39;azienda
[**InvoicesCsv**](AziendeApi.md#invoicescsv) | **GET** /aziende/{idAzienda}/fatture.csv | Recupera l&#39;elenco delle fatture dell&#39;azienda in formato CSV
[**InvoicesZip**](AziendeApi.md#invoiceszip) | **GET** /aziende/{idAzienda}/fatture.zip | Recupera uno zip delle fatture dell&#39;azienda
[**List**](AziendeApi.md#list) | **GET** /aziende | Recupera le aziende visibili dall&#39;utente
[**ListPdd**](AziendeApi.md#listpdd) | **GET** /aziende/{idAzienda}/pdd | Recupera l&#39;elenco delle richieste di pacchetti di distribuzione per l&#39;azienda
[**ListPdv**](AziendeApi.md#listpdv) | **GET** /aziende/{idAzienda}/pdv | Recupera l&#39;elenco dei pacchetto di versamento caricati dall&#39;azienda
[**MetadataQNames**](AziendeApi.md#metadataqnames) | **GET** /aziende/{idAzienda}/metadati | Recupera l&#39;elenco dei metadati dell&#39;azienda
[**Pda**](AziendeApi.md#pda) | **GET** /aziende/{idAzienda}/pda | Recupera l&#39;elenco dei pacchetti di archiviazione dell&#39;azienda
[**Read**](AziendeApi.md#read) | **GET** /aziende/{idAzienda} | Recupera le informazioni di una azienda
[**Receipts**](AziendeApi.md#receipts) | **GET** /aziende/{idAzienda}/ricevute | Recupera l&#39;elenco delle ricevute SDI relative alle fatture dell&#39;azienda
[**RemoveParameter**](AziendeApi.md#removeparameter) | **DELETE** /aziende/{idAzienda}/parametri/{nome} | Rimuove un parametro dall&#39;azienda
[**RemoveTag**](AziendeApi.md#removetag) | **DELETE** /aziende/{idAzienda}/tag | Rimuove un&#39;etichetta dall&#39;azienda
[**RemoveUser**](AziendeApi.md#removeuser) | **PUT** /aziende/{idAzienda}/utenti/{chiaveUtente}/remove | Rimuove un utente dall&#39;azienda
[**Revoke**](AziendeApi.md#revoke) | **POST** /aziende/{idAzienda}/revoca | Porta l&#39;azienda nello stato delega revocata
[**SingleInvoices**](AziendeApi.md#singleinvoices) | **GET** /aziende/{idAzienda}/fattureSingole | Recupera l&#39;elenco delle fatture singole dell&#39;azienda
[**SingleInvoicesCsv**](AziendeApi.md#singleinvoicescsv) | **GET** /aziende/{idAzienda}/fattureSingole.csv | Recupera l&#39;elenco delle fatture singole dell&#39;azienda in formato CSV
[**StatusTotalizations**](AziendeApi.md#statustotalizations) | **GET** /aziende/totaliStati | Recupera i totali per stati delle aziende
[**SubmitToHallRegistry**](AziendeApi.md#submittohallregistry) | **POST** /aziende/{idAzienda}/hall | Crea un nuovo pacchetto di versamento di tipo F998
[**Suspend**](AziendeApi.md#suspend) | **POST** /aziende/{idAzienda}/sospendi | Porta l&#39;azienda nello stato sospeso
[**Tags**](AziendeApi.md#tags) | **GET** /aziende/{idAzienda}/tag | Recupera l&#39;elenco delle etichette di una azienda
[**Totalizations**](AziendeApi.md#totalizations) | **GET** /aziende/{idAzienda}/totali | Recupera i totali dell&#39;azienda
[**Update**](AziendeApi.md#update) | **PUT** /aziende/{idAzienda} | Aggiorna le informazioni di una azienda
[**Users**](AziendeApi.md#users) | **GET** /aziende/{idAzienda}/utenti | Recupera l&#39;elenco degli utenti associati all&#39;azienda
[**Wait**](AziendeApi.md#wait) | **POST** /aziende/{idAzienda}/attendi | Porta l&#39;azienda nello stato attesa delega


<a name="activate"></a>
# **Activate**
> Info Activate (string idAzienda, string body = null)

Porta l'azienda nello stato attivo

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ActivateExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = body_example;  // string | Note (optional) 

            try
            {
                // Porta l'azienda nello stato attivo
                Info result = apiInstance.Activate(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Activate: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | **string**| Note | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="addparameter"></a>
# **AddParameter**
> Info AddParameter (string idAzienda, string nome, string body = null)

Aggiunge un parametro all'azienda

Aggiunge un parametro all'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class AddParameterExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var nome = nome_example;  // string | Nome del parametro
            var body = body_example;  // string |  (optional) 

            try
            {
                // Aggiunge un parametro all'azienda
                Info result = apiInstance.AddParameter(idAzienda, nome, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.AddParameter: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **nome** | **string**| Nome del parametro | 
 **body** | **string**|  | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="addtag"></a>
# **AddTag**
> Info AddTag (string idAzienda, string body = null)

Aggiunge un'etichetta all'azienda

Aggiunge un'etichetta all'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class AddTagExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = body_example;  // string | Etichetta da aggiungere (optional) 

            try
            {
                // Aggiunge un'etichetta all'azienda
                Info result = apiInstance.AddTag(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.AddTag: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | **string**| Etichetta da aggiungere | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="adduser"></a>
# **AddUser**
> Info AddUser (string chiaveAzienda, string chiaveUtente, string body = null)

Associa un utente all'azienda

Associa un utente all'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class AddUserExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var chiaveAzienda = chiaveAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var chiaveUtente = chiaveUtente_example;  // string | Identificativo o indirizzo email dell'utente
            var body = body_example;  // string | Note (optional) 

            try
            {
                // Associa un utente all'azienda
                Info result = apiInstance.AddUser(chiaveAzienda, chiaveUtente, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.AddUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **chiaveAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **chiaveUtente** | **string**| Identificativo o indirizzo email dell&#39;utente | 
 **body** | **string**| Note | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createcompany"></a>
# **CreateCompany**
> Azienda CreateCompany (Azienda body = null, bool? oneCompanyPerUser = null)

Crea una nuova azienda

Crea una nuova azienda utilizzando le informazioni di default dell'utente

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateCompanyExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var body = new Azienda(); // Azienda | Dati dell'azienda da creare (optional) 
            var oneCompanyPerUser = true;  // bool? | Determina se è permessa la creazione di più aziende con lo stesso utente (optional) 

            try
            {
                // Crea una nuova azienda
                Azienda result = apiInstance.CreateCompany(body, oneCompanyPerUser);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.CreateCompany: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**Azienda**](Azienda.md)| Dati dell&#39;azienda da creare | [optional] 
 **oneCompanyPerUser** | **bool?**| Determina se è permessa la creazione di più aziende con lo stesso utente | [optional] 

### Return type

[**Azienda**](Azienda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createfromfile"></a>
# **CreateFromFile**
> Fattura CreateFromFile (string idAzienda, System.IO.Stream invoice)

Crea una nuova fattura caricando il file XML

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateFromFileExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var invoice = new System.IO.Stream(); // System.IO.Stream | File contenente la fattura

            try
            {
                // Crea una nuova fattura caricando il file XML
                Fattura result = apiInstance.CreateFromFile(idAzienda, invoice);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.CreateFromFile: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **invoice** | **System.IO.Stream**| File contenente la fattura | 

### Return type

[**Fattura**](Fattura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createinvoice"></a>
# **CreateInvoice**
> Fattura CreateInvoice (string idAzienda, Fattura body = null)

Crea una nuova fattura

Crea una nuova fattura per l'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateInvoiceExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = new Fattura(); // Fattura | Dati della nuova fattura (optional) 

            try
            {
                // Crea una nuova fattura
                Fattura result = apiInstance.CreateInvoice(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.CreateInvoice: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | [**Fattura**](Fattura.md)| Dati della nuova fattura | [optional] 

### Return type

[**Fattura**](Fattura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createinvoicefromxml"></a>
# **CreateInvoiceFromXml**
> Fattura CreateInvoiceFromXml (string idAzienda, List<byte[]> body = null)

Crea una nuova fattura per l'azienda caricando il file XML

Il servizio accetta fatture nel formato XML FatturaPA ricavando l'azienda dalla partita IVA indicata nei dati del Cedente/Prestatore.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateInvoiceFromXmlExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = ;  // List<byte[]> | xml (optional) 

            try
            {
                // Crea una nuova fattura per l'azienda caricando il file XML
                Fattura result = apiInstance.CreateInvoiceFromXml(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.CreateInvoiceFromXml: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | **List&lt;byte[]&gt;**| xml | [optional] 

### Return type

[**Fattura**](Fattura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createpdd"></a>
# **CreatePdd**
> Pdd CreatePdd (string idAzienda, Pdd body = null, string salva = null)

Crea una nuova richiesta di pacchetto di distribuzione per l'azienda

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreatePddExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = new Pdd(); // Pdd | Informazioni sul pacchetto di versamento da creare (optional) 
            var salva = salva_example;  // string | Flag che indica se la richiesta è da salvare o da lasciare in bozza (optional) 

            try
            {
                // Crea una nuova richiesta di pacchetto di distribuzione per l'azienda
                Pdd result = apiInstance.CreatePdd(idAzienda, body, salva);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.CreatePdd: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | [**Pdd**](Pdd.md)| Informazioni sul pacchetto di versamento da creare | [optional] 
 **salva** | **string**| Flag che indica se la richiesta è da salvare o da lasciare in bozza | [optional] 

### Return type

[**Pdd**](Pdd.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createpdv"></a>
# **CreatePdv**
> Pdv CreatePdv (string idAzienda, System.IO.Stream pdv, string dir = null)

Crea un nuovo pacchetto di versamento di Fatture Elettroniche per l'azienda caricando il file ZIP

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreatePdvExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var pdv = new System.IO.Stream(); // System.IO.Stream | File del pacchetto di versamento Fatture Elettroniche
            var dir = dir_example;  // string | Indica se il pacchetto contiene fatture trasmesse o ricevute (optional) 

            try
            {
                // Crea un nuovo pacchetto di versamento di Fatture Elettroniche per l'azienda caricando il file ZIP
                Pdv result = apiInstance.CreatePdv(idAzienda, pdv, dir);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.CreatePdv: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **pdv** | **System.IO.Stream**| File del pacchetto di versamento Fatture Elettroniche | 
 **dir** | **string**| Indica se il pacchetto contiene fatture trasmesse o ricevute | [optional] 

### Return type

[**Pdv**](Pdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createpdvfromfile"></a>
# **CreatePdvFromFile**
> Pdv CreatePdvFromFile (string idAzienda, string pdvFormat, System.IO.Stream pdv)

Crea un nuovo pacchetto di versamento in uno specifico formato caricando il file ZIP con il contenuto

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreatePdvFromFileExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo del produttore
            var pdvFormat = pdvFormat_example;  // string | Formato del pacchetto di versamento
            var pdv = new System.IO.Stream(); // System.IO.Stream | File ZIP con il contenuto del pacchetto di versamento

            try
            {
                // Crea un nuovo pacchetto di versamento in uno specifico formato caricando il file ZIP con il contenuto
                Pdv result = apiInstance.CreatePdvFromFile(idAzienda, pdvFormat, pdv);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.CreatePdvFromFile: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo del produttore | 
 **pdvFormat** | **string**| Formato del pacchetto di versamento | 
 **pdv** | **System.IO.Stream**| File ZIP con il contenuto del pacchetto di versamento | 

### Return type

[**Pdv**](Pdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createpdv_0"></a>
# **CreatePdv_0**
> Pdv CreatePdv_0 (string idAzienda, Pdv body = null)

Crea un nuovo pacchetto di versamento per l'azienda

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreatePdv_0Example
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = new Pdv(); // Pdv | Informazioni sul pacchetto di versamento da creare (optional) 

            try
            {
                // Crea un nuovo pacchetto di versamento per l'azienda
                Pdv result = apiInstance.CreatePdv_0(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.CreatePdv_0: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | [**Pdv**](Pdv.md)| Informazioni sul pacchetto di versamento da creare | [optional] 

### Return type

[**Pdv**](Pdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="createreceivedinvoices"></a>
# **CreateReceivedInvoices**
> Fattura CreateReceivedInvoices (string idAzienda, System.IO.Stream file)

Invia le fatture ricevute sul sito AdE caricando i file xml o p7m

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class CreateReceivedInvoicesExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var file = new System.IO.Stream(); // System.IO.Stream | File contenente la fattura o la notifica MT

            try
            {
                // Invia le fatture ricevute sul sito AdE caricando i file xml o p7m
                Fattura result = apiInstance.CreateReceivedInvoices(idAzienda, file);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.CreateReceivedInvoices: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **file** | **System.IO.Stream**| File contenente la fattura o la notifica MT | 

### Return type

[**Fattura**](Fattura.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="error"></a>
# **Error**
> Info Error (string idAzienda, string body = null)

Porta l'azienda nello stato delega errata

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ErrorExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = body_example;  // string | Note (optional) 

            try
            {
                // Porta l'azienda nello stato delega errata
                Info result = apiInstance.Error(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Error: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | **string**| Note | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getparameters"></a>
# **GetParameters**
> Parametro GetParameters (string idAzienda, string nome)

Recupera l'elenco dei parametri di configurazione di un'azienda

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetParametersExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var nome = nome_example;  // string | Nome del parametro di configurazione

            try
            {
                // Recupera l'elenco dei parametri di configurazione di un'azienda
                Parametro result = apiInstance.GetParameters(idAzienda, nome);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.GetParameters: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **nome** | **string**| Nome del parametro di configurazione | 

### Return type

[**Parametro**](Parametro.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getparameters_0"></a>
# **GetParameters_0**
> Parametri GetParameters_0 (string idAzienda)

Recupera l'elenco dei parametri di configurazione di un'azienda

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetParameters_0Example
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda

            try
            {
                // Recupera l'elenco dei parametri di configurazione di un'azienda
                Parametri result = apiInstance.GetParameters_0(idAzienda);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.GetParameters_0: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 

### Return type

[**Parametri**](Parametri.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="getroles"></a>
# **GetRoles**
> Roles GetRoles (string idAzienda)

Recupera l'elenco dei ruoli dell'utente su di un'azienda

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class GetRolesExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda

            try
            {
                // Recupera l'elenco dei ruoli dell'utente su di un'azienda
                Roles result = apiInstance.GetRoles(idAzienda);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.GetRoles: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 

### Return type

[**Roles**](Roles.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="history"></a>
# **History**
> ElencoStoriciAzienda History (string idAzienda, int? inizio = null, int? max = null)

Storico dell'azienda

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class HistoryExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Storico dell'azienda
                ElencoStoriciAzienda result = apiInstance.History(idAzienda, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.History: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoStoriciAzienda**](ElencoStoriciAzienda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="invoiceoutcometotalizations"></a>
# **InvoiceOutcomeTotalizations**
> ElencoValori InvoiceOutcomeTotalizations (string idAzienda, string dir = null)

Recupera i totali per esiti delle fatture dell'azienda

Recupera i totali dell'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class InvoiceOutcomeTotalizationsExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 

            try
            {
                // Recupera i totali per esiti delle fatture dell'azienda
                ElencoValori result = apiInstance.InvoiceOutcomeTotalizations(idAzienda, dir);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.InvoiceOutcomeTotalizations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 

### Return type

[**ElencoValori**](ElencoValori.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="invoicestatustotalizations"></a>
# **InvoiceStatusTotalizations**
> ElencoValori InvoiceStatusTotalizations (string idAzienda)

Recupera i totali per stati delle fatture dell'azienda

Recupera i totali dell'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class InvoiceStatusTotalizationsExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda

            try
            {
                // Recupera i totali per stati delle fatture dell'azienda
                ElencoValori result = apiInstance.InvoiceStatusTotalizations(idAzienda);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.InvoiceStatusTotalizations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 

### Return type

[**ElencoValori**](ElencoValori.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="invoices"></a>
# **Invoices**
> ElencoFatture Invoices (string idAzienda, int? inizio = null, int? max = null, string dataInizio = null, string dataFine = null, string dataIrInizio = null, string dataIrFine = null, string numeroDocumento = null, string numero = null, string sezionale = null, string nomeFile = null, string destinatario = null, string mittente = null, string importo = null, string dir = null, string status = null, string esito = null, string schema = null, string flag = null, string i = null, List<string> s = null)

Recupera l'elenco delle fatture dell'azienda

Recupera l'elenco delle fatture dell'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class InvoicesExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var dataInizio = dataInizio_example;  // string | Data documento inizio (yyyy-mm-dd) (optional) 
            var dataFine = dataFine_example;  // string | Data documento fine (yyyy-mm-dd) (optional) 
            var dataIrInizio = dataIrInizio_example;  // string | Data invio/ricezione inizio (yyyy-mm-dd) (optional) 
            var dataIrFine = dataIrFine_example;  // string | Data invio/ricezione fine (yyyy-mm-dd) (optional) 
            var numeroDocumento = numeroDocumento_example;  // string | Ricerca per numero documento (optional) 
            var numero = numero_example;  // string | Ricerca per numero fattura (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale fattura (optional) 
            var nomeFile = nomeFile_example;  // string | Ricerca per nome file (optional) 
            var destinatario = destinatario_example;  // string | Ricerca per destinatario (optional) 
            var mittente = mittente_example;  // string | Ricerca per mittente (optional) 
            var importo = importo_example;  // string | Ricerca per importo (optional) 
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 
            var status = status_example;  // string | Stato fattura (optional) 
            var esito = esito_example;  // string | Esito fattura (optional) 
            var schema = schema_example;  // string | Schema fattura (optional) 
            var flag = flag_example;  // string | Flag modificato (optional) 
            var i = i_example;  // string | Campi supplementari da includere (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco delle fatture dell'azienda
                ElencoFatture result = apiInstance.Invoices(idAzienda, inizio, max, dataInizio, dataFine, dataIrInizio, dataIrFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, dir, status, esito, schema, flag, i, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Invoices: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **dataInizio** | **string**| Data documento inizio (yyyy-mm-dd) | [optional] 
 **dataFine** | **string**| Data documento fine (yyyy-mm-dd) | [optional] 
 **dataIrInizio** | **string**| Data invio/ricezione inizio (yyyy-mm-dd) | [optional] 
 **dataIrFine** | **string**| Data invio/ricezione fine (yyyy-mm-dd) | [optional] 
 **numeroDocumento** | **string**| Ricerca per numero documento | [optional] 
 **numero** | **string**| Ricerca per numero fattura | [optional] 
 **sezionale** | **string**| Ricerca per sezionale fattura | [optional] 
 **nomeFile** | **string**| Ricerca per nome file | [optional] 
 **destinatario** | **string**| Ricerca per destinatario | [optional] 
 **mittente** | **string**| Ricerca per mittente | [optional] 
 **importo** | **string**| Ricerca per importo | [optional] 
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 
 **status** | **string**| Stato fattura | [optional] 
 **esito** | **string**| Esito fattura | [optional] 
 **schema** | **string**| Schema fattura | [optional] 
 **flag** | **string**| Flag modificato | [optional] 
 **i** | **string**| Campi supplementari da includere | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoFatture**](ElencoFatture.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="invoicescsv"></a>
# **InvoicesCsv**
> System.IO.Stream InvoicesCsv (string idAzienda, string anno = null, string dataInizio = null, string dataFine = null, string dataIrInizio = null, string dataIrFine = null, string numeroDocumento = null, string numero = null, string sezionale = null, string nomeFile = null, string destinatario = null, string mittente = null, string importo = null, string dir = null, string status = null, string esito = null, string schema = null, string flag = null, List<string> s = null, List<string> d = null)

Recupera l'elenco delle fatture dell'azienda in formato CSV

Recupera l'elenco delle fatture dell'azienda in formato CSV.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class InvoicesCsvExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var anno = anno_example;  // string | Ricerca per anno (optional) 
            var dataInizio = dataInizio_example;  // string | Data documento inizio (yyyy-mm-dd) (optional) 
            var dataFine = dataFine_example;  // string | Data documento fine (yyyy-mm-dd) (optional) 
            var dataIrInizio = dataIrInizio_example;  // string | Data invio/ricezione inizio (yyyy-mm-dd) (optional) 
            var dataIrFine = dataIrFine_example;  // string | Data invio/ricezione fine (yyyy-mm-dd) (optional) 
            var numeroDocumento = numeroDocumento_example;  // string | Ricerca per numero documento (optional) 
            var numero = numero_example;  // string | Ricerca per numero fattura (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale fattura (optional) 
            var nomeFile = nomeFile_example;  // string | Ricerca per nome file (optional) 
            var destinatario = destinatario_example;  // string | Ricerca per destinatario (optional) 
            var mittente = mittente_example;  // string | Ricerca per mittente (optional) 
            var importo = importo_example;  // string | Ricerca per importo (optional) 
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 
            var status = status_example;  // string | Stato fattura (optional) 
            var esito = esito_example;  // string | Esito fattura (optional) 
            var schema = schema_example;  // string | Schema fattura (optional) 
            var flag = flag_example;  // string | Flag modificato (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 
            var d = new List<string>(); // List<string> | Dato aggiuntivo (optional) 

            try
            {
                // Recupera l'elenco delle fatture dell'azienda in formato CSV
                System.IO.Stream result = apiInstance.InvoicesCsv(idAzienda, anno, dataInizio, dataFine, dataIrInizio, dataIrFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, dir, status, esito, schema, flag, s, d);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.InvoicesCsv: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **anno** | **string**| Ricerca per anno | [optional] 
 **dataInizio** | **string**| Data documento inizio (yyyy-mm-dd) | [optional] 
 **dataFine** | **string**| Data documento fine (yyyy-mm-dd) | [optional] 
 **dataIrInizio** | **string**| Data invio/ricezione inizio (yyyy-mm-dd) | [optional] 
 **dataIrFine** | **string**| Data invio/ricezione fine (yyyy-mm-dd) | [optional] 
 **numeroDocumento** | **string**| Ricerca per numero documento | [optional] 
 **numero** | **string**| Ricerca per numero fattura | [optional] 
 **sezionale** | **string**| Ricerca per sezionale fattura | [optional] 
 **nomeFile** | **string**| Ricerca per nome file | [optional] 
 **destinatario** | **string**| Ricerca per destinatario | [optional] 
 **mittente** | **string**| Ricerca per mittente | [optional] 
 **importo** | **string**| Ricerca per importo | [optional] 
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 
 **status** | **string**| Stato fattura | [optional] 
 **esito** | **string**| Esito fattura | [optional] 
 **schema** | **string**| Schema fattura | [optional] 
 **flag** | **string**| Flag modificato | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 
 **d** | [**List&lt;string&gt;**](string.md)| Dato aggiuntivo | [optional] 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: text/csv, application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="invoiceszip"></a>
# **InvoicesZip**
> System.IO.Stream InvoicesZip (string idAzienda, string dataInizio = null, string dataFine = null, string dataIrInizio = null, string dataIrFine = null, string numeroDocumento = null, string numero = null, string sezionale = null, string nomeFile = null, string destinatario = null, string mittente = null, string importo = null, string dir = null, string status = null, string esito = null, string schema = null, string i = null, List<string> d = null)

Recupera uno zip delle fatture dell'azienda

Recupera uno zip delle fatture dell'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class InvoicesZipExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var dataInizio = dataInizio_example;  // string | Data documento inizio (yyyy-mm-dd) (optional) 
            var dataFine = dataFine_example;  // string | Data documento fine (yyyy-mm-dd) (optional) 
            var dataIrInizio = dataIrInizio_example;  // string | Data invio/ricezione inizio (yyyy-mm-dd) (optional) 
            var dataIrFine = dataIrFine_example;  // string | Data invio/ricezione fine (yyyy-mm-dd) (optional) 
            var numeroDocumento = numeroDocumento_example;  // string | Ricerca per numero documento (optional) 
            var numero = numero_example;  // string | Ricerca per numero fattura (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale fattura (optional) 
            var nomeFile = nomeFile_example;  // string | Ricerca per nome file (optional) 
            var destinatario = destinatario_example;  // string | Ricerca per destinatario (optional) 
            var mittente = mittente_example;  // string | Ricerca per mittente (optional) 
            var importo = importo_example;  // string | Ricerca per importo (optional) 
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 
            var status = status_example;  // string | Stato fattura (optional) 
            var esito = esito_example;  // string | Esito fattura (optional) 
            var schema = schema_example;  // string | Schema fattura (optional) 
            var i = i_example;  // string | File aggiuntivi da includere (optional) 
            var d = new List<string>(); // List<string> | Dato aggiuntivo (optional) 

            try
            {
                // Recupera uno zip delle fatture dell'azienda
                System.IO.Stream result = apiInstance.InvoicesZip(idAzienda, dataInizio, dataFine, dataIrInizio, dataIrFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, dir, status, esito, schema, i, d);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.InvoicesZip: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **dataInizio** | **string**| Data documento inizio (yyyy-mm-dd) | [optional] 
 **dataFine** | **string**| Data documento fine (yyyy-mm-dd) | [optional] 
 **dataIrInizio** | **string**| Data invio/ricezione inizio (yyyy-mm-dd) | [optional] 
 **dataIrFine** | **string**| Data invio/ricezione fine (yyyy-mm-dd) | [optional] 
 **numeroDocumento** | **string**| Ricerca per numero documento | [optional] 
 **numero** | **string**| Ricerca per numero fattura | [optional] 
 **sezionale** | **string**| Ricerca per sezionale fattura | [optional] 
 **nomeFile** | **string**| Ricerca per nome file | [optional] 
 **destinatario** | **string**| Ricerca per destinatario | [optional] 
 **mittente** | **string**| Ricerca per mittente | [optional] 
 **importo** | **string**| Ricerca per importo | [optional] 
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 
 **status** | **string**| Stato fattura | [optional] 
 **esito** | **string**| Esito fattura | [optional] 
 **schema** | **string**| Schema fattura | [optional] 
 **i** | **string**| File aggiuntivi da includere | [optional] 
 **d** | [**List&lt;string&gt;**](string.md)| Dato aggiuntivo | [optional] 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/octet-stream, application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="list"></a>
# **List**
> ElencoAziende List (int? inizio = null, int? max = null, string q = null, string email = null, string emailPec = null, string stato = null, List<string> s = null)

Recupera le aziende visibili dall'utente

Recupera l'elenco delle aziende visibili dall'utente collegato.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var q = q_example;  // string | Filtro per ragione sociale, partita iva e codice fiscale (optional) 
            var email = email_example;  // string | Filtro per email (optional) 
            var emailPec = emailPec_example;  // string | Filtro per pec (optional) 
            var stato = stato_example;  // string | Filtro per stato azienda (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera le aziende visibili dall'utente
                ElencoAziende result = apiInstance.List(inizio, max, q, email, emailPec, stato, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.List: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **q** | **string**| Filtro per ragione sociale, partita iva e codice fiscale | [optional] 
 **email** | **string**| Filtro per email | [optional] 
 **emailPec** | **string**| Filtro per pec | [optional] 
 **stato** | **string**| Filtro per stato azienda | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoAziende**](ElencoAziende.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listpdd"></a>
# **ListPdd**
> ElencoPdv ListPdd (string idAzienda, string tipo = null, string stato = null, string descrizione = null, int? inizio = null, int? max = null, List<string> s = null)

Recupera l'elenco delle richieste di pacchetti di distribuzione per l'azienda

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListPddExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var tipo = tipo_example;  // string | Tipo della richiesta  (optional) 
            var stato = stato_example;  // string | Stato della richiesta  (optional) 
            var descrizione = descrizione_example;  // string | Filtro per descrizione (optional) 
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco delle richieste di pacchetti di distribuzione per l'azienda
                ElencoPdv result = apiInstance.ListPdd(idAzienda, tipo, stato, descrizione, inizio, max, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.ListPdd: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **tipo** | **string**| Tipo della richiesta  | [optional] 
 **stato** | **string**| Stato della richiesta  | [optional] 
 **descrizione** | **string**| Filtro per descrizione | [optional] 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoPdv**](ElencoPdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="listpdv"></a>
# **ListPdv**
> ElencoPdv ListPdv (string idAzienda, string dataInizio = null, string dataFine = null, string stato = null, string formato = null, int? inizio = null, int? max = null, List<string> s = null)

Recupera l'elenco dei pacchetto di versamento caricati dall'azienda

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ListPdvExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var dataInizio = dataInizio_example;  // string | Data inizio selezione ISO-8601 (optional) 
            var dataFine = dataFine_example;  // string | Data fine selezione ISO-8601 (optional) 
            var stato = stato_example;  // string | Stato del pacchetto (optional) 
            var formato = formato_example;  // string | Formato del pacchetto (optional) 
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco dei pacchetto di versamento caricati dall'azienda
                ElencoPdv result = apiInstance.ListPdv(idAzienda, dataInizio, dataFine, stato, formato, inizio, max, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.ListPdv: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **dataInizio** | **string**| Data inizio selezione ISO-8601 | [optional] 
 **dataFine** | **string**| Data fine selezione ISO-8601 | [optional] 
 **stato** | **string**| Stato del pacchetto | [optional] 
 **formato** | **string**| Formato del pacchetto | [optional] 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoPdv**](ElencoPdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="metadataqnames"></a>
# **MetadataQNames**
> ElencoMetadati MetadataQNames (string idAzienda)

Recupera l'elenco dei metadati dell'azienda

Recupera l'elenco dei metadati dell'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class MetadataQNamesExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda

            try
            {
                // Recupera l'elenco dei metadati dell'azienda
                ElencoMetadati result = apiInstance.MetadataQNames(idAzienda);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.MetadataQNames: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 

### Return type

[**ElencoMetadati**](ElencoMetadati.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="pda"></a>
# **Pda**
> ElencoPda Pda (string idAzienda, int? inizio = null, int? max = null, string numero = null, string sezionale = null, string tipoDocumento = null, string anno = null, string stato = null, List<string> s = null)

Recupera l'elenco dei pacchetti di archiviazione dell'azienda

Recupera l'elenco dei pacchetti di archiviazione dell'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class PdaExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo dell'azienda
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var numero = numero_example;  // string | Ricerca per numero pacchetto (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale (optional) 
            var tipoDocumento = tipoDocumento_example;  // string | Ricerca per tipo documento (optional) 
            var anno = anno_example;  // string | Ricerca per anno (optional) 
            var stato = stato_example;  // string | Stato pda (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 

            try
            {
                // Recupera l'elenco dei pacchetti di archiviazione dell'azienda
                ElencoPda result = apiInstance.Pda(idAzienda, inizio, max, numero, sezionale, tipoDocumento, anno, stato, s);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Pda: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo dell&#39;azienda | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **numero** | **string**| Ricerca per numero pacchetto | [optional] 
 **sezionale** | **string**| Ricerca per sezionale | [optional] 
 **tipoDocumento** | **string**| Ricerca per tipo documento | [optional] 
 **anno** | **string**| Ricerca per anno | [optional] 
 **stato** | **string**| Stato pda | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 

### Return type

[**ElencoPda**](ElencoPda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="read"></a>
# **Read**
> Azienda Read (string idAzienda)

Recupera le informazioni di una azienda

Recupera le informazioni di un'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReadExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda

            try
            {
                // Recupera le informazioni di una azienda
                Azienda result = apiInstance.Read(idAzienda);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Read: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 

### Return type

[**Azienda**](Azienda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="receipts"></a>
# **Receipts**
> ElencoRicevute Receipts (string idAzienda, int? inizio = null, int? max = null, string dataInizio = null, string dataFine = null, string tipo = null, string flag = null, string i = null)

Recupera l'elenco delle ricevute SDI relative alle fatture dell'azienda

Recupera l'elenco delle ricevute provenienti dal Sistema di Interscambio per le fatture dell'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class ReceiptsExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var dataInizio = dataInizio_example;  // string | Data inizio selezione ISO-8601 (optional) 
            var dataFine = dataFine_example;  // string | Data fine selezione ISO-8601 (optional) 
            var tipo = tipo_example;  // string | Tipo ricevuta (optional) 
            var flag = flag_example;  // string | Flag lettura (optional) 
            var i = i_example;  // string | Campi supplementari da includere (optional) 

            try
            {
                // Recupera l'elenco delle ricevute SDI relative alle fatture dell'azienda
                ElencoRicevute result = apiInstance.Receipts(idAzienda, inizio, max, dataInizio, dataFine, tipo, flag, i);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Receipts: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **dataInizio** | **string**| Data inizio selezione ISO-8601 | [optional] 
 **dataFine** | **string**| Data fine selezione ISO-8601 | [optional] 
 **tipo** | **string**| Tipo ricevuta | [optional] 
 **flag** | **string**| Flag lettura | [optional] 
 **i** | **string**| Campi supplementari da includere | [optional] 

### Return type

[**ElencoRicevute**](ElencoRicevute.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removeparameter"></a>
# **RemoveParameter**
> Info RemoveParameter (string idAzienda, string nome)

Rimuove un parametro dall'azienda

Rimuove un parametro dall'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RemoveParameterExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo dell'azienda
            var nome = nome_example;  // string | Nome del parametro

            try
            {
                // Rimuove un parametro dall'azienda
                Info result = apiInstance.RemoveParameter(idAzienda, nome);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.RemoveParameter: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo dell&#39;azienda | 
 **nome** | **string**| Nome del parametro | 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removetag"></a>
# **RemoveTag**
> Info RemoveTag (string idAzienda, string body = null)

Rimuove un'etichetta dall'azienda

Rimuove un'etichetta dall'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RemoveTagExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = body_example;  // string | Etichetta da rimuovere (optional) 

            try
            {
                // Rimuove un'etichetta dall'azienda
                Info result = apiInstance.RemoveTag(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.RemoveTag: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | **string**| Etichetta da rimuovere | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="removeuser"></a>
# **RemoveUser**
> Info RemoveUser (long? idAzienda, string chiaveUtente, string body = null)

Rimuove un utente dall'azienda

Rimuove l'associazione di un utente all'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RemoveUserExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = 789;  // long? | Identificativo dell'azienda
            var chiaveUtente = chiaveUtente_example;  // string | Identificativo o indirizzo email dell'utente
            var body = body_example;  // string | Note (optional) 

            try
            {
                // Rimuove un utente dall'azienda
                Info result = apiInstance.RemoveUser(idAzienda, chiaveUtente, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.RemoveUser: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **long?**| Identificativo dell&#39;azienda | 
 **chiaveUtente** | **string**| Identificativo o indirizzo email dell&#39;utente | 
 **body** | **string**| Note | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="revoke"></a>
# **Revoke**
> Info Revoke (string idAzienda, string body = null)

Porta l'azienda nello stato delega revocata

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class RevokeExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = body_example;  // string | Note (optional) 

            try
            {
                // Porta l'azienda nello stato delega revocata
                Info result = apiInstance.Revoke(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Revoke: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | **string**| Note | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="singleinvoices"></a>
# **SingleInvoices**
> ElencoFattureSingole SingleInvoices (string idAzienda, int? inizio = null, int? max = null, string dataInizio = null, string dataFine = null, string dataIrInizio = null, string dataIrFine = null, string numeroDocumento = null, string numero = null, string sezionale = null, string nomeFile = null, string destinatario = null, string mittente = null, string importo = null, string q = null, string dir = null, string status = null, string esito = null, string schema = null, string flag = null, List<string> i = null, List<string> s = null, List<string> d = null)

Recupera l'elenco delle fatture singole dell'azienda

Recupera l'elenco delle fatture singole dell'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class SingleInvoicesExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)
            var dataInizio = dataInizio_example;  // string | Data documento inizio (yyyy-mm-dd) (optional) 
            var dataFine = dataFine_example;  // string | Data documento fine (yyyy-mm-dd) (optional) 
            var dataIrInizio = dataIrInizio_example;  // string | Data invio/ricezione inizio (yyyy-mm-dd) (optional) 
            var dataIrFine = dataIrFine_example;  // string | Data invio/ricezione fine (yyyy-mm-dd) (optional) 
            var numeroDocumento = numeroDocumento_example;  // string | Ricerca per numero documento (optional) 
            var numero = numero_example;  // string | Ricerca per numero fattura (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale fattura (optional) 
            var nomeFile = nomeFile_example;  // string | Ricerca per nome file (optional) 
            var destinatario = destinatario_example;  // string | Ricerca per destinatario (optional) 
            var mittente = mittente_example;  // string | Ricerca per mittente (optional) 
            var importo = importo_example;  // string | Ricerca per importo (optional) 
            var q = q_example;  // string | Ricerca a testo libero (optional) 
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 
            var status = status_example;  // string | Stato fattura (optional) 
            var esito = esito_example;  // string | Esito fattura (optional) 
            var schema = schema_example;  // string | Schema fattura (optional) 
            var flag = flag_example;  // string | Flag modificato (optional) 
            var i = i_example;  // List<string> | Campi supplementari da includere (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 
            var d = new List<string>(); // List<string> | Dato aggiuntivo (optional) 

            try
            {
                // Recupera l'elenco delle fatture singole dell'azienda
                ElencoFattureSingole result = apiInstance.SingleInvoices(idAzienda, inizio, max, dataInizio, dataFine, dataIrInizio, dataIrFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, q, dir, status, esito, schema, flag, i, s, d);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.SingleInvoices: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]
 **dataInizio** | **string**| Data documento inizio (yyyy-mm-dd) | [optional] 
 **dataFine** | **string**| Data documento fine (yyyy-mm-dd) | [optional] 
 **dataIrInizio** | **string**| Data invio/ricezione inizio (yyyy-mm-dd) | [optional] 
 **dataIrFine** | **string**| Data invio/ricezione fine (yyyy-mm-dd) | [optional] 
 **numeroDocumento** | **string**| Ricerca per numero documento | [optional] 
 **numero** | **string**| Ricerca per numero fattura | [optional] 
 **sezionale** | **string**| Ricerca per sezionale fattura | [optional] 
 **nomeFile** | **string**| Ricerca per nome file | [optional] 
 **destinatario** | **string**| Ricerca per destinatario | [optional] 
 **mittente** | **string**| Ricerca per mittente | [optional] 
 **importo** | **string**| Ricerca per importo | [optional] 
 **q** | **string**| Ricerca a testo libero | [optional] 
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 
 **status** | **string**| Stato fattura | [optional] 
 **esito** | **string**| Esito fattura | [optional] 
 **schema** | **string**| Schema fattura | [optional] 
 **flag** | **string**| Flag modificato | [optional] 
 **i** | **List&lt;string&gt;**| Campi supplementari da includere | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 
 **d** | [**List&lt;string&gt;**](string.md)| Dato aggiuntivo | [optional] 

### Return type

[**ElencoFattureSingole**](ElencoFattureSingole.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="singleinvoicescsv"></a>
# **SingleInvoicesCsv**
> System.IO.Stream SingleInvoicesCsv (string idAzienda, string anno = null, string dataInizio = null, string dataFine = null, string dataIrInizio = null, string dataIrFine = null, string numeroDocumento = null, string numero = null, string sezionale = null, string nomeFile = null, string destinatario = null, string mittente = null, string importo = null, string q = null, string dir = null, string status = null, string esito = null, string schema = null, string flag = null, List<string> s = null, List<string> d = null)

Recupera l'elenco delle fatture singole dell'azienda in formato CSV

Recupera l'elenco delle fatture singole dell'azienda in formato CSV.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class SingleInvoicesCsvExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var anno = anno_example;  // string | Ricerca per anno (optional) 
            var dataInizio = dataInizio_example;  // string | Data documento inizio (yyyy-mm-dd) (optional) 
            var dataFine = dataFine_example;  // string | Data documento fine (yyyy-mm-dd) (optional) 
            var dataIrInizio = dataIrInizio_example;  // string | Data invio/ricezione inizio (yyyy-mm-dd) (optional) 
            var dataIrFine = dataIrFine_example;  // string | Data invio/ricezione fine (yyyy-mm-dd) (optional) 
            var numeroDocumento = numeroDocumento_example;  // string | Ricerca per numero documento (optional) 
            var numero = numero_example;  // string | Ricerca per numero fattura (optional) 
            var sezionale = sezionale_example;  // string | Ricerca per sezionale fattura (optional) 
            var nomeFile = nomeFile_example;  // string | Ricerca per nome file (optional) 
            var destinatario = destinatario_example;  // string | Ricerca per destinatario (optional) 
            var mittente = mittente_example;  // string | Ricerca per mittente (optional) 
            var importo = importo_example;  // string | Ricerca per importo (optional) 
            var q = q_example;  // string | Ricerca a testo libero (optional) 
            var dir = dir_example;  // string | Ricerca per fatture trasmesse o ricevute (optional) 
            var status = status_example;  // string | Stato fattura (optional) 
            var esito = esito_example;  // string | Esito fattura (optional) 
            var schema = schema_example;  // string | Schema fattura (optional) 
            var flag = flag_example;  // string | Flag modificato (optional) 
            var s = new List<string>(); // List<string> | Criterio di ordinamento (optional) 
            var d = new List<string>(); // List<string> | Dato aggiuntivo (optional) 

            try
            {
                // Recupera l'elenco delle fatture singole dell'azienda in formato CSV
                System.IO.Stream result = apiInstance.SingleInvoicesCsv(idAzienda, anno, dataInizio, dataFine, dataIrInizio, dataIrFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, q, dir, status, esito, schema, flag, s, d);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.SingleInvoicesCsv: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **anno** | **string**| Ricerca per anno | [optional] 
 **dataInizio** | **string**| Data documento inizio (yyyy-mm-dd) | [optional] 
 **dataFine** | **string**| Data documento fine (yyyy-mm-dd) | [optional] 
 **dataIrInizio** | **string**| Data invio/ricezione inizio (yyyy-mm-dd) | [optional] 
 **dataIrFine** | **string**| Data invio/ricezione fine (yyyy-mm-dd) | [optional] 
 **numeroDocumento** | **string**| Ricerca per numero documento | [optional] 
 **numero** | **string**| Ricerca per numero fattura | [optional] 
 **sezionale** | **string**| Ricerca per sezionale fattura | [optional] 
 **nomeFile** | **string**| Ricerca per nome file | [optional] 
 **destinatario** | **string**| Ricerca per destinatario | [optional] 
 **mittente** | **string**| Ricerca per mittente | [optional] 
 **importo** | **string**| Ricerca per importo | [optional] 
 **q** | **string**| Ricerca a testo libero | [optional] 
 **dir** | **string**| Ricerca per fatture trasmesse o ricevute | [optional] 
 **status** | **string**| Stato fattura | [optional] 
 **esito** | **string**| Esito fattura | [optional] 
 **schema** | **string**| Schema fattura | [optional] 
 **flag** | **string**| Flag modificato | [optional] 
 **s** | [**List&lt;string&gt;**](string.md)| Criterio di ordinamento | [optional] 
 **d** | [**List&lt;string&gt;**](string.md)| Dato aggiuntivo | [optional] 

### Return type

**System.IO.Stream**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: text/csv, application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="statustotalizations"></a>
# **StatusTotalizations**
> ElencoValori StatusTotalizations ()

Recupera i totali per stati delle aziende

Recupera i totali per stati delle aziende.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class StatusTotalizationsExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();

            try
            {
                // Recupera i totali per stati delle aziende
                ElencoValori result = apiInstance.StatusTotalizations();
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.StatusTotalizations: " + e.Message );
            }
        }
    }
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ElencoValori**](ElencoValori.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="submittohallregistry"></a>
# **SubmitToHallRegistry**
> Pdv SubmitToHallRegistry (string idAzienda, System.IO.Stream pdv)

Crea un nuovo pacchetto di versamento di tipo F998

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class SubmitToHallRegistryExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var pdv = new System.IO.Stream(); // System.IO.Stream | File con il contenuto del pacchetto di versamento

            try
            {
                // Crea un nuovo pacchetto di versamento di tipo F998
                Pdv result = apiInstance.SubmitToHallRegistry(idAzienda, pdv);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.SubmitToHallRegistry: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **pdv** | **System.IO.Stream**| File con il contenuto del pacchetto di versamento | 

### Return type

[**Pdv**](Pdv.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="suspend"></a>
# **Suspend**
> Info Suspend (string idAzienda, string body = null)

Porta l'azienda nello stato sospeso

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class SuspendExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = body_example;  // string | Note (optional) 

            try
            {
                // Porta l'azienda nello stato sospeso
                Info result = apiInstance.Suspend(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Suspend: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | **string**| Note | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="tags"></a>
# **Tags**
> Etichette Tags (string idAzienda)

Recupera l'elenco delle etichette di una azienda

Recupera l'elenco delle etichette di una azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class TagsExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda

            try
            {
                // Recupera l'elenco delle etichette di una azienda
                Etichette result = apiInstance.Tags(idAzienda);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Tags: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 

### Return type

[**Etichette**](Etichette.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="totalizations"></a>
# **Totalizations**
> Totali Totalizations (string idAzienda)

Recupera i totali dell'azienda

Recupera i totali dell'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class TotalizationsExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda

            try
            {
                // Recupera i totali dell'azienda
                Totali result = apiInstance.Totalizations(idAzienda);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Totalizations: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 

### Return type

[**Totali**](Totali.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="update"></a>
# **Update**
> Azienda Update (string idAzienda, Azienda body = null)

Aggiorna le informazioni di una azienda

Aggiorna le informazioni di un'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UpdateExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = new Azienda(); // Azienda |  (optional) 

            try
            {
                // Aggiorna le informazioni di una azienda
                Azienda result = apiInstance.Update(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Update: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | [**Azienda**](Azienda.md)|  | [optional] 

### Return type

[**Azienda**](Azienda.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="users"></a>
# **Users**
> ElencoUtenti Users (string idAzienda, int? inizio = null, int? max = null)

Recupera l'elenco degli utenti associati all'azienda

Recupera l'elenco degli utenti associati all'azienda.

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class UsersExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var inizio = 56;  // int? | Posizione del primo elemento restituito (optional)  (default to 0)
            var max = 56;  // int? | Numero massimo di elementi (optional)  (default to 50)

            try
            {
                // Recupera l'elenco degli utenti associati all'azienda
                ElencoUtenti result = apiInstance.Users(idAzienda, inizio, max);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Users: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **inizio** | **int?**| Posizione del primo elemento restituito | [optional] [default to 0]
 **max** | **int?**| Numero massimo di elementi | [optional] [default to 50]

### Return type

[**ElencoUtenti**](ElencoUtenti.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

<a name="wait"></a>
# **Wait**
> Info Wait (string idAzienda, string body = null)

Porta l'azienda nello stato attesa delega

### Example
```csharp
using System;
using System.Diagnostics;
using Entaksi.eCon.Api;
using Entaksi.eCon.Client;
using Entaksi.eCon.Model;

namespace Example
{
    public class WaitExample
    {
        public void main()
        {
            var apiInstance = new AziendeApi();
            var idAzienda = idAzienda_example;  // string | Identificativo o partita IVA dell'azienda
            var body = body_example;  // string | Note (optional) 

            try
            {
                // Porta l'azienda nello stato attesa delega
                Info result = apiInstance.Wait(idAzienda, body);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling AziendeApi.Wait: " + e.Message );
            }
        }
    }
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **idAzienda** | **string**| Identificativo o partita IVA dell&#39;azienda | 
 **body** | **string**| Note | [optional] 

### Return type

[**Info**](Info.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/json, application/xml

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

