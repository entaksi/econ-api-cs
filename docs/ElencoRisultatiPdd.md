# Entaksi.eCon.Model.ElencoRisultatiPdd
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Paginazione** | [**Paginazione**](Paginazione.md) |  | [optional] 
**ListaRisultatiPdd** | [**List&lt;RisultatoPdd&gt;**](RisultatoPdd.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

