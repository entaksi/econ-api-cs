# Entaksi.eCon.Model.ElencoPdv
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Paginazione** | [**Paginazione**](Paginazione.md) |  | [optional] 
**ListaPdv** | [**List&lt;Pdv&gt;**](Pdv.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

