# Entaksi.eCon.Model.ElencoValori
## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Valori** | [**List&lt;StringProperty&gt;**](StringProperty.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

