/* 
 * Entaksi eDoc API
 *
 * API REST per l'accesso ai servizi di archiviazione elettronica e gestione della fattura elettronica Entaksi Solutions.
 *
 * OpenAPI spec version: 1.14.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = Entaksi.eCon.Client.SwaggerDateConverter;

namespace Entaksi.eCon.Model
{
    /// <summary>
    /// Metadato
    /// </summary>
    [DataContract]
    public partial class Metadato :  IEquatable<Metadato>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Metadato" /> class.
        /// </summary>
        /// <param name="nome">Nome del metadato..</param>
        /// <param name="valore">Titolo del metadato..</param>
        public Metadato(string nome = default(string), string valore = default(string))
        {
            this.Nome = nome;
            this.Valore = valore;
        }
        
        /// <summary>
        /// Nome del metadato.
        /// </summary>
        /// <value>Nome del metadato.</value>
        [DataMember(Name="nome", EmitDefaultValue=false)]
        public string Nome { get; set; }

        /// <summary>
        /// Titolo del metadato.
        /// </summary>
        /// <value>Titolo del metadato.</value>
        [DataMember(Name="valore", EmitDefaultValue=false)]
        public string Valore { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Metadato {\n");
            sb.Append("  Nome: ").Append(Nome).Append("\n");
            sb.Append("  Valore: ").Append(Valore).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Metadato);
        }

        /// <summary>
        /// Returns true if Metadato instances are equal
        /// </summary>
        /// <param name="input">Instance of Metadato to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Metadato input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.Nome == input.Nome ||
                    (this.Nome != null &&
                    this.Nome.Equals(input.Nome))
                ) && 
                (
                    this.Valore == input.Valore ||
                    (this.Valore != null &&
                    this.Valore.Equals(input.Valore))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.Nome != null)
                    hashCode = hashCode * 59 + this.Nome.GetHashCode();
                if (this.Valore != null)
                    hashCode = hashCode * 59 + this.Valore.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
