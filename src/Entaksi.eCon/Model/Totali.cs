/* 
 * Entaksi eDoc API
 *
 * API REST per l'accesso ai servizi di archiviazione elettronica e gestione della fattura elettronica Entaksi Solutions.
 *
 * OpenAPI spec version: 1.14.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.ComponentModel.DataAnnotations;
using SwaggerDateConverter = Entaksi.eCon.Client.SwaggerDateConverter;

namespace Entaksi.eCon.Model
{
    /// <summary>
    /// Valori dei contatori per la misurazione dell&#39;uso del servizio
    /// </summary>
    [DataContract]
    public partial class Totali :  IEquatable<Totali>, IValidatableObject
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Totali" /> class.
        /// </summary>
        /// <param name="spazioOccupato">Spazio occupato nel sistema di archiviazione.</param>
        /// <param name="documenti">Numero di unità documentarie nel sistema di archiviazione.</param>
        /// <param name="file">Numero di file nel sistema di archiviazione.</param>
        /// <param name="documentiScarto">Numero di unità documentarie nel sistema di archiviazione destinate allo scarto nei prossimi 6 mesi.</param>
        /// <param name="spazioScarto">Spazio occupato nel sistema di archiviazione destinato allo scarto nei prossimi 6 mesi.</param>
        public Totali(long? spazioOccupato = default(long?), long? documenti = default(long?), long? file = default(long?), long? documentiScarto = default(long?), long? spazioScarto = default(long?))
        {
            this.SpazioOccupato = spazioOccupato;
            this.Documenti = documenti;
            this.File = file;
            this.DocumentiScarto = documentiScarto;
            this.SpazioScarto = spazioScarto;
        }
        
        /// <summary>
        /// Spazio occupato nel sistema di archiviazione
        /// </summary>
        /// <value>Spazio occupato nel sistema di archiviazione</value>
        [DataMember(Name="spazio_occupato", EmitDefaultValue=false)]
        public long? SpazioOccupato { get; set; }

        /// <summary>
        /// Numero di unità documentarie nel sistema di archiviazione
        /// </summary>
        /// <value>Numero di unità documentarie nel sistema di archiviazione</value>
        [DataMember(Name="documenti", EmitDefaultValue=false)]
        public long? Documenti { get; set; }

        /// <summary>
        /// Numero di file nel sistema di archiviazione
        /// </summary>
        /// <value>Numero di file nel sistema di archiviazione</value>
        [DataMember(Name="file", EmitDefaultValue=false)]
        public long? File { get; set; }

        /// <summary>
        /// Numero di unità documentarie nel sistema di archiviazione destinate allo scarto nei prossimi 6 mesi
        /// </summary>
        /// <value>Numero di unità documentarie nel sistema di archiviazione destinate allo scarto nei prossimi 6 mesi</value>
        [DataMember(Name="documenti_scarto", EmitDefaultValue=false)]
        public long? DocumentiScarto { get; set; }

        /// <summary>
        /// Spazio occupato nel sistema di archiviazione destinato allo scarto nei prossimi 6 mesi
        /// </summary>
        /// <value>Spazio occupato nel sistema di archiviazione destinato allo scarto nei prossimi 6 mesi</value>
        [DataMember(Name="spazio_scarto", EmitDefaultValue=false)]
        public long? SpazioScarto { get; set; }

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class Totali {\n");
            sb.Append("  SpazioOccupato: ").Append(SpazioOccupato).Append("\n");
            sb.Append("  Documenti: ").Append(Documenti).Append("\n");
            sb.Append("  File: ").Append(File).Append("\n");
            sb.Append("  DocumentiScarto: ").Append(DocumentiScarto).Append("\n");
            sb.Append("  SpazioScarto: ").Append(SpazioScarto).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }
  
        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="input">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object input)
        {
            return this.Equals(input as Totali);
        }

        /// <summary>
        /// Returns true if Totali instances are equal
        /// </summary>
        /// <param name="input">Instance of Totali to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(Totali input)
        {
            if (input == null)
                return false;

            return 
                (
                    this.SpazioOccupato == input.SpazioOccupato ||
                    (this.SpazioOccupato != null &&
                    this.SpazioOccupato.Equals(input.SpazioOccupato))
                ) && 
                (
                    this.Documenti == input.Documenti ||
                    (this.Documenti != null &&
                    this.Documenti.Equals(input.Documenti))
                ) && 
                (
                    this.File == input.File ||
                    (this.File != null &&
                    this.File.Equals(input.File))
                ) && 
                (
                    this.DocumentiScarto == input.DocumentiScarto ||
                    (this.DocumentiScarto != null &&
                    this.DocumentiScarto.Equals(input.DocumentiScarto))
                ) && 
                (
                    this.SpazioScarto == input.SpazioScarto ||
                    (this.SpazioScarto != null &&
                    this.SpazioScarto.Equals(input.SpazioScarto))
                );
        }

        /// <summary>
        /// Gets the hash code
        /// </summary>
        /// <returns>Hash code</returns>
        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hashCode = 41;
                if (this.SpazioOccupato != null)
                    hashCode = hashCode * 59 + this.SpazioOccupato.GetHashCode();
                if (this.Documenti != null)
                    hashCode = hashCode * 59 + this.Documenti.GetHashCode();
                if (this.File != null)
                    hashCode = hashCode * 59 + this.File.GetHashCode();
                if (this.DocumentiScarto != null)
                    hashCode = hashCode * 59 + this.DocumentiScarto.GetHashCode();
                if (this.SpazioScarto != null)
                    hashCode = hashCode * 59 + this.SpazioScarto.GetHashCode();
                return hashCode;
            }
        }

        /// <summary>
        /// To validate all properties of the instance
        /// </summary>
        /// <param name="validationContext">Validation context</param>
        /// <returns>Validation Result</returns>
        IEnumerable<System.ComponentModel.DataAnnotations.ValidationResult> IValidatableObject.Validate(ValidationContext validationContext)
        {
            yield break;
        }
    }

}
