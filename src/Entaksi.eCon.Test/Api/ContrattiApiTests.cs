/* 
 * Entaksi eDoc API
 *
 * API REST per l'accesso ai servizi di archiviazione elettronica e gestione della fattura elettronica Entaksi Solutions.
 *
 * OpenAPI spec version: 1.14.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using Entaksi.eCon.Client;
using Entaksi.eCon.Api;
using Entaksi.eCon.Model;

namespace Entaksi.eCon.Test
{
    /// <summary>
    ///  Class for testing ContrattiApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class ContrattiApiTests
    {
        private ContrattiApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new ContrattiApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of ContrattiApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' ContrattiApi
            //Assert.IsInstanceOfType(typeof(ContrattiApi), instance, "instance is a ContrattiApi");
        }

        
        /// <summary>
        /// Test AddTag
        /// </summary>
        [Test]
        public void AddTagTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //string body = null;
            //var response = instance.AddTag(idContratto, body);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test AddUser
        /// </summary>
        [Test]
        public void AddUserTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //string chiaveUtente = null;
            //var response = instance.AddUser(idContratto, chiaveUtente);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test Companies
        /// </summary>
        [Test]
        public void CompaniesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //int? inizio = null;
            //int? max = null;
            //var response = instance.Companies(idContratto, inizio, max);
            //Assert.IsInstanceOf<ElencoAziende> (response, "response is ElencoAziende");
        }
        
        /// <summary>
        /// Test Flag
        /// </summary>
        [Test]
        public void FlagTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //string tag = null;
            //var response = instance.Flag(idContratto, tag);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test GetParameters
        /// </summary>
        [Test]
        public void GetParametersTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //var response = instance.GetParameters(idContratto);
            //Assert.IsInstanceOf<Parametri> (response, "response is Parametri");
        }
        
        /// <summary>
        /// Test GetParameters_0
        /// </summary>
        [Test]
        public void GetParameters_0Test()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //string nome = null;
            //var response = instance.GetParameters_0(idContratto, nome);
            //Assert.IsInstanceOf<Parametro> (response, "response is Parametro");
        }
        
        /// <summary>
        /// Test GetRoles
        /// </summary>
        [Test]
        public void GetRolesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //var response = instance.GetRoles(idContratto);
            //Assert.IsInstanceOf<Roles> (response, "response is Roles");
        }
        
        /// <summary>
        /// Test List
        /// </summary>
        [Test]
        public void ListTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? inizio = null;
            //int? max = null;
            //List<string> s = null;
            //var response = instance.List(inizio, max, s);
            //Assert.IsInstanceOf<ElencoContratti> (response, "response is ElencoContratti");
        }
        
        /// <summary>
        /// Test NewCompany
        /// </summary>
        [Test]
        public void NewCompanyTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //Azienda body = null;
            //var response = instance.NewCompany(idContratto, body);
            //Assert.IsInstanceOf<Azienda> (response, "response is Azienda");
        }
        
        /// <summary>
        /// Test Read
        /// </summary>
        [Test]
        public void ReadTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //var response = instance.Read(idContratto);
            //Assert.IsInstanceOf<Contratto> (response, "response is Contratto");
        }
        
        /// <summary>
        /// Test RemoveTag
        /// </summary>
        [Test]
        public void RemoveTagTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //string body = null;
            //var response = instance.RemoveTag(idContratto, body);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test RemoveUser
        /// </summary>
        [Test]
        public void RemoveUserTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //string chiaveUtente = null;
            //var response = instance.RemoveUser(idContratto, chiaveUtente);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test Tags
        /// </summary>
        [Test]
        public void TagsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //var response = instance.Tags(idContratto);
            //Assert.IsInstanceOf<Etichette> (response, "response is Etichette");
        }
        
        /// <summary>
        /// Test Totalizations
        /// </summary>
        [Test]
        public void TotalizationsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //var response = instance.Totalizations(idContratto);
            //Assert.IsInstanceOf<Totali> (response, "response is Totali");
        }
        
        /// <summary>
        /// Test Update
        /// </summary>
        [Test]
        public void UpdateTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //Contratto body = null;
            //var response = instance.Update(idContratto, body);
            //Assert.IsInstanceOf<Contratto> (response, "response is Contratto");
        }
        
        /// <summary>
        /// Test Users
        /// </summary>
        [Test]
        public void UsersTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //long? idContratto = null;
            //int? inizio = null;
            //int? max = null;
            //var response = instance.Users(idContratto, inizio, max);
            //Assert.IsInstanceOf<ElencoUtenti> (response, "response is ElencoUtenti");
        }
        
    }

}
