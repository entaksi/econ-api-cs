/* 
 * Entaksi eDoc API
 *
 * API REST per l'accesso ai servizi di archiviazione elettronica e gestione della fattura elettronica Entaksi Solutions.
 *
 * OpenAPI spec version: 1.14.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */

using System;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using RestSharp;
using NUnit.Framework;

using Entaksi.eCon.Client;
using Entaksi.eCon.Api;
using Entaksi.eCon.Model;

namespace Entaksi.eCon.Test
{
    /// <summary>
    ///  Class for testing FattureApi
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the API endpoint.
    /// </remarks>
    [TestFixture]
    public class FattureApiTests
    {
        private FattureApi instance;

        /// <summary>
        /// Setup before each unit test
        /// </summary>
        [SetUp]
        public void Init()
        {
            instance = new FattureApi();
        }

        /// <summary>
        /// Clean up after each unit test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of FattureApi
        /// </summary>
        [Test]
        public void InstanceTest()
        {
            // TODO uncomment below to test 'IsInstanceOfType' FattureApi
            //Assert.IsInstanceOfType(typeof(FattureApi), instance, "instance is a FattureApi");
        }

        
        /// <summary>
        /// Test AddNamedValue
        /// </summary>
        [Test]
        public void AddNamedValueTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //Dato body = null;
            //var response = instance.AddNamedValue(idFattura, dir, body);
            //Assert.IsInstanceOf<Dato> (response, "response is Dato");
        }
        
        /// <summary>
        /// Test Cancel
        /// </summary>
        [Test]
        public void CancelTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //var response = instance.Cancel(idFattura);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test ChangeNamedValue
        /// </summary>
        [Test]
        public void ChangeNamedValueTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string nome = null;
            //string dir = null;
            //Dato body = null;
            //var response = instance.ChangeNamedValue(idFattura, nome, dir, body);
            //Assert.IsInstanceOf<Dato> (response, "response is Dato");
        }
        
        /// <summary>
        /// Test ConfirmEC
        /// </summary>
        [Test]
        public void ConfirmECTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //var response = instance.ConfirmEC(idFattura);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test CreateFromXml
        /// </summary>
        [Test]
        public void CreateFromXmlTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //List<byte[]> body = null;
            //var response = instance.CreateFromXml(body);
            //Assert.IsInstanceOf<Fattura> (response, "response is Fattura");
        }
        
        /// <summary>
        /// Test CreateReceipt
        /// </summary>
        [Test]
        public void CreateReceiptTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //Ricevuta body = null;
            //var response = instance.CreateReceipt(idFattura, body);
            //Assert.IsInstanceOf<Ricevuta> (response, "response is Ricevuta");
        }
        
        /// <summary>
        /// Test Csv
        /// </summary>
        [Test]
        public void CsvTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string anno = null;
            //string dataInizio = null;
            //string dataFine = null;
            //string numeroDocumento = null;
            //string numero = null;
            //string sezionale = null;
            //string nomeFile = null;
            //string destinatario = null;
            //string mittente = null;
            //string importo = null;
            //string dir = null;
            //string status = null;
            //string esito = null;
            //string schema = null;
            //string flag = null;
            //List<string> s = null;
            //List<string> d = null;
            //var response = instance.Csv(anno, dataInizio, dataFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, dir, status, esito, schema, flag, s, d);
            //Assert.IsInstanceOf<System.IO.Stream> (response, "response is System.IO.Stream");
        }
        
        /// <summary>
        /// Test DeleteNamedValue
        /// </summary>
        [Test]
        public void DeleteNamedValueTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string nome = null;
            //string dir = null;
            //var response = instance.DeleteNamedValue(idFattura, nome, dir);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test Flag
        /// </summary>
        [Test]
        public void FlagTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //var response = instance.Flag(idFattura, dir);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test GetNamedValue
        /// </summary>
        [Test]
        public void GetNamedValueTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string nome = null;
            //string dir = null;
            //var response = instance.GetNamedValue(idFattura, nome, dir);
            //Assert.IsInstanceOf<Dato> (response, "response is Dato");
        }
        
        /// <summary>
        /// Test History
        /// </summary>
        [Test]
        public void HistoryTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //int? inizio = null;
            //int? max = null;
            //var response = instance.History(idFattura, dir, inizio, max);
            //Assert.IsInstanceOf<ElencoStoriciFattura> (response, "response is ElencoStoriciFattura");
        }
        
        /// <summary>
        /// Test List
        /// </summary>
        [Test]
        public void ListTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //int? inizio = null;
            //int? max = null;
            //string dataInizio = null;
            //string dataFine = null;
            //string dataIrInizio = null;
            //string dataIrFine = null;
            //string numeroDocumento = null;
            //string numero = null;
            //string sezionale = null;
            //string nomeFile = null;
            //string destinatario = null;
            //string mittente = null;
            //string importo = null;
            //string dir = null;
            //string status = null;
            //string esito = null;
            //string schema = null;
            //string flag = null;
            //string i = null;
            //List<string> s = null;
            //var response = instance.List(inizio, max, dataInizio, dataFine, dataIrInizio, dataIrFine, numeroDocumento, numero, sezionale, nomeFile, destinatario, mittente, importo, dir, status, esito, schema, flag, i, s);
            //Assert.IsInstanceOf<ElencoFatture> (response, "response is ElencoFatture");
        }
        
        /// <summary>
        /// Test ListNamedValues
        /// </summary>
        [Test]
        public void ListNamedValuesTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //int? inizio = null;
            //int? max = null;
            //var response = instance.ListNamedValues(idFattura, dir, inizio, max);
            //Assert.IsInstanceOf<ElencoDati> (response, "response is ElencoDati");
        }
        
        /// <summary>
        /// Test OutcomeInvoiceTotalizations
        /// </summary>
        [Test]
        public void OutcomeInvoiceTotalizationsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string dir = null;
            //var response = instance.OutcomeInvoiceTotalizations(dir);
            //Assert.IsInstanceOf<ElencoValori> (response, "response is ElencoValori");
        }
        
        /// <summary>
        /// Test Read
        /// </summary>
        [Test]
        public void ReadTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //string i = null;
            //var response = instance.Read(idFattura, dir, i);
            //Assert.IsInstanceOf<Fattura> (response, "response is Fattura");
        }
        
        /// <summary>
        /// Test Receipts
        /// </summary>
        [Test]
        public void ReceiptsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //int? inizio = null;
            //int? max = null;
            //string dataInizio = null;
            //string dataFine = null;
            //string tipo = null;
            //string flag = null;
            //var response = instance.Receipts(idFattura, dir, inizio, max, dataInizio, dataFine, tipo, flag);
            //Assert.IsInstanceOf<ElencoRicevute> (response, "response is ElencoRicevute");
        }
        
        /// <summary>
        /// Test RemoveReceipts
        /// </summary>
        [Test]
        public void RemoveReceiptsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //long? idElemento = null;
            //var response = instance.RemoveReceipts(idFattura, idElemento);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test Resume
        /// </summary>
        [Test]
        public void ResumeTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //var response = instance.Resume(idFattura);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test RetrieveP7m
        /// </summary>
        [Test]
        public void RetrieveP7mTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //var response = instance.RetrieveP7m(idFattura, dir);
            //Assert.IsInstanceOf<System.IO.Stream> (response, "response is System.IO.Stream");
        }
        
        /// <summary>
        /// Test RetrievePdf
        /// </summary>
        [Test]
        public void RetrievePdfTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //string stile = null;
            //var response = instance.RetrievePdf(idFattura, dir, stile);
            //Assert.IsInstanceOf<System.IO.Stream> (response, "response is System.IO.Stream");
        }
        
        /// <summary>
        /// Test RetrieveXml
        /// </summary>
        [Test]
        public void RetrieveXmlTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //var response = instance.RetrieveXml(idFattura, dir);
            //Assert.IsInstanceOf<System.IO.Stream> (response, "response is System.IO.Stream");
        }
        
        /// <summary>
        /// Test RetrieveXmlDeprecated
        /// </summary>
        [Test]
        public void RetrieveXmlDeprecatedTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //var response = instance.RetrieveXmlDeprecated(idFattura, dir);
            //Assert.IsInstanceOf<string> (response, "response is string");
        }
        
        /// <summary>
        /// Test Send
        /// </summary>
        [Test]
        public void SendTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //var response = instance.Send(idFattura);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test StatusInvoiceTotalizations
        /// </summary>
        [Test]
        public void StatusInvoiceTotalizationsTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //var response = instance.StatusInvoiceTotalizations();
            //Assert.IsInstanceOf<ElencoValori> (response, "response is ElencoValori");
        }
        
        /// <summary>
        /// Test Unflag
        /// </summary>
        [Test]
        public void UnflagTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //var response = instance.Unflag(idFattura, dir);
            //Assert.IsInstanceOf<Info> (response, "response is Info");
        }
        
        /// <summary>
        /// Test Update
        /// </summary>
        [Test]
        public void UpdateTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //Fattura body = null;
            //var response = instance.Update(idFattura, dir, body);
            //Assert.IsInstanceOf<Fattura> (response, "response is Fattura");
        }
        
        /// <summary>
        /// Test UpdateXml
        /// </summary>
        [Test]
        public void UpdateXmlTest()
        {
            // TODO uncomment below to test the method and replace null with proper value
            //string idFattura = null;
            //string dir = null;
            //List<byte[]> body = null;
            //var response = instance.UpdateXml(idFattura, dir, body);
            //Assert.IsInstanceOf<Fattura> (response, "response is Fattura");
        }
        
    }

}
