/* 
 * Entaksi eDoc API
 *
 * API REST per l'accesso ai servizi di archiviazione elettronica e gestione della fattura elettronica Entaksi Solutions.
 *
 * OpenAPI spec version: 1.14.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Entaksi.eCon.Api;
using Entaksi.eCon.Model;
using Entaksi.eCon.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Entaksi.eCon.Test
{
    /// <summary>
    ///  Class for testing Totali
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class TotaliTests
    {
        // TODO uncomment below to declare an instance variable for Totali
        //private Totali instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of Totali
            //instance = new Totali();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of Totali
        /// </summary>
        [Test]
        public void TotaliInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" Totali
            //Assert.IsInstanceOfType<Totali> (instance, "variable 'instance' is a Totali");
        }


        /// <summary>
        /// Test the property 'SpazioOccupato'
        /// </summary>
        [Test]
        public void SpazioOccupatoTest()
        {
            // TODO unit test for the property 'SpazioOccupato'
        }
        /// <summary>
        /// Test the property 'Documenti'
        /// </summary>
        [Test]
        public void DocumentiTest()
        {
            // TODO unit test for the property 'Documenti'
        }
        /// <summary>
        /// Test the property 'File'
        /// </summary>
        [Test]
        public void FileTest()
        {
            // TODO unit test for the property 'File'
        }
        /// <summary>
        /// Test the property 'DocumentiScarto'
        /// </summary>
        [Test]
        public void DocumentiScartoTest()
        {
            // TODO unit test for the property 'DocumentiScarto'
        }
        /// <summary>
        /// Test the property 'SpazioScarto'
        /// </summary>
        [Test]
        public void SpazioScartoTest()
        {
            // TODO unit test for the property 'SpazioScarto'
        }

    }

}
