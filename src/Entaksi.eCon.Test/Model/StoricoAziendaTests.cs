/* 
 * Entaksi eDoc API
 *
 * API REST per l'accesso ai servizi di archiviazione elettronica e gestione della fattura elettronica Entaksi Solutions.
 *
 * OpenAPI spec version: 1.14.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Entaksi.eCon.Api;
using Entaksi.eCon.Model;
using Entaksi.eCon.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Entaksi.eCon.Test
{
    /// <summary>
    ///  Class for testing StoricoAzienda
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class StoricoAziendaTests
    {
        // TODO uncomment below to declare an instance variable for StoricoAzienda
        //private StoricoAzienda instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of StoricoAzienda
            //instance = new StoricoAzienda();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of StoricoAzienda
        /// </summary>
        [Test]
        public void StoricoAziendaInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" StoricoAzienda
            //Assert.IsInstanceOfType<StoricoAzienda> (instance, "variable 'instance' is a StoricoAzienda");
        }


        /// <summary>
        /// Test the property 'Links'
        /// </summary>
        [Test]
        public void LinksTest()
        {
            // TODO unit test for the property 'Links'
        }
        /// <summary>
        /// Test the property 'Id'
        /// </summary>
        [Test]
        public void IdTest()
        {
            // TODO unit test for the property 'Id'
        }
        /// <summary>
        /// Test the property 'IdAzienda'
        /// </summary>
        [Test]
        public void IdAziendaTest()
        {
            // TODO unit test for the property 'IdAzienda'
        }
        /// <summary>
        /// Test the property 'IdUtente'
        /// </summary>
        [Test]
        public void IdUtenteTest()
        {
            // TODO unit test for the property 'IdUtente'
        }
        /// <summary>
        /// Test the property 'Utente'
        /// </summary>
        [Test]
        public void UtenteTest()
        {
            // TODO unit test for the property 'Utente'
        }
        /// <summary>
        /// Test the property 'DataOperazione'
        /// </summary>
        [Test]
        public void DataOperazioneTest()
        {
            // TODO unit test for the property 'DataOperazione'
        }
        /// <summary>
        /// Test the property 'Stato'
        /// </summary>
        [Test]
        public void StatoTest()
        {
            // TODO unit test for the property 'Stato'
        }

    }

}
