/* 
 * Entaksi eDoc API
 *
 * API REST per l'accesso ai servizi di archiviazione elettronica e gestione della fattura elettronica Entaksi Solutions.
 *
 * OpenAPI spec version: 1.14.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Entaksi.eCon.Api;
using Entaksi.eCon.Model;
using Entaksi.eCon.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Entaksi.eCon.Test
{
    /// <summary>
    ///  Class for testing ModelClient
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class ModelClientTests
    {
        // TODO uncomment below to declare an instance variable for ModelClient
        //private ModelClient instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of ModelClient
            //instance = new ModelClient();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of ModelClient
        /// </summary>
        [Test]
        public void ModelClientInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" ModelClient
            //Assert.IsInstanceOfType<ModelClient> (instance, "variable 'instance' is a ModelClient");
        }


        /// <summary>
        /// Test the property 'IdClient'
        /// </summary>
        [Test]
        public void IdClientTest()
        {
            // TODO unit test for the property 'IdClient'
        }

    }

}
