/* 
 * Entaksi eDoc API
 *
 * API REST per l'accesso ai servizi di archiviazione elettronica e gestione della fattura elettronica Entaksi Solutions.
 *
 * OpenAPI spec version: 1.14.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Entaksi.eCon.Api;
using Entaksi.eCon.Model;
using Entaksi.eCon.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Entaksi.eCon.Test
{
    /// <summary>
    ///  Class for testing PredicatoPdd
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class PredicatoPddTests
    {
        // TODO uncomment below to declare an instance variable for PredicatoPdd
        //private PredicatoPdd instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of PredicatoPdd
            //instance = new PredicatoPdd();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of PredicatoPdd
        /// </summary>
        [Test]
        public void PredicatoPddInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" PredicatoPdd
            //Assert.IsInstanceOfType<PredicatoPdd> (instance, "variable 'instance' is a PredicatoPdd");
        }


        /// <summary>
        /// Test the property 'Metadato'
        /// </summary>
        [Test]
        public void MetadatoTest()
        {
            // TODO unit test for the property 'Metadato'
        }
        /// <summary>
        /// Test the property 'Valore'
        /// </summary>
        [Test]
        public void ValoreTest()
        {
            // TODO unit test for the property 'Valore'
        }
        /// <summary>
        /// Test the property 'Operatore'
        /// </summary>
        [Test]
        public void OperatoreTest()
        {
            // TODO unit test for the property 'Operatore'
        }

    }

}
