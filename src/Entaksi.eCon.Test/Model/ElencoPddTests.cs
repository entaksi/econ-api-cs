/* 
 * Entaksi eDoc API
 *
 * API REST per l'accesso ai servizi di archiviazione elettronica e gestione della fattura elettronica Entaksi Solutions.
 *
 * OpenAPI spec version: 1.14.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Entaksi.eCon.Api;
using Entaksi.eCon.Model;
using Entaksi.eCon.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Entaksi.eCon.Test
{
    /// <summary>
    ///  Class for testing ElencoPdd
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class ElencoPddTests
    {
        // TODO uncomment below to declare an instance variable for ElencoPdd
        //private ElencoPdd instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of ElencoPdd
            //instance = new ElencoPdd();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of ElencoPdd
        /// </summary>
        [Test]
        public void ElencoPddInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" ElencoPdd
            //Assert.IsInstanceOfType<ElencoPdd> (instance, "variable 'instance' is a ElencoPdd");
        }


        /// <summary>
        /// Test the property 'Paginazione'
        /// </summary>
        [Test]
        public void PaginazioneTest()
        {
            // TODO unit test for the property 'Paginazione'
        }
        /// <summary>
        /// Test the property 'ListaPdd'
        /// </summary>
        [Test]
        public void ListaPddTest()
        {
            // TODO unit test for the property 'ListaPdd'
        }

    }

}
