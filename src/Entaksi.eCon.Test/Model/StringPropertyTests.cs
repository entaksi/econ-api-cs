/* 
 * Entaksi eDoc API
 *
 * API REST per l'accesso ai servizi di archiviazione elettronica e gestione della fattura elettronica Entaksi Solutions.
 *
 * OpenAPI spec version: 1.14.3
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 */


using NUnit.Framework;

using System;
using System.Linq;
using System.IO;
using System.Collections.Generic;
using Entaksi.eCon.Api;
using Entaksi.eCon.Model;
using Entaksi.eCon.Client;
using System.Reflection;
using Newtonsoft.Json;

namespace Entaksi.eCon.Test
{
    /// <summary>
    ///  Class for testing StringProperty
    /// </summary>
    /// <remarks>
    /// This file is automatically generated by Swagger Codegen.
    /// Please update the test case below to test the model.
    /// </remarks>
    [TestFixture]
    public class StringPropertyTests
    {
        // TODO uncomment below to declare an instance variable for StringProperty
        //private StringProperty instance;

        /// <summary>
        /// Setup before each test
        /// </summary>
        [SetUp]
        public void Init()
        {
            // TODO uncomment below to create an instance of StringProperty
            //instance = new StringProperty();
        }

        /// <summary>
        /// Clean up after each test
        /// </summary>
        [TearDown]
        public void Cleanup()
        {

        }

        /// <summary>
        /// Test an instance of StringProperty
        /// </summary>
        [Test]
        public void StringPropertyInstanceTest()
        {
            // TODO uncomment below to test "IsInstanceOfType" StringProperty
            //Assert.IsInstanceOfType<StringProperty> (instance, "variable 'instance' is a StringProperty");
        }


        /// <summary>
        /// Test the property 'Name'
        /// </summary>
        [Test]
        public void NameTest()
        {
            // TODO unit test for the property 'Name'
        }
        /// <summary>
        /// Test the property 'Value'
        /// </summary>
        [Test]
        public void ValueTest()
        {
            // TODO unit test for the property 'Value'
        }

    }

}
